<?php

namespace App\Controllers;

use Exception;

class Laporan_harian extends BaseController
{
	public function index()
	{
		$data['list_data'] = [];
		if(!empty($this->request->getGet("jenis_laporan"))){
			if($this->request->getGet("jenis_laporan") == "1"){
				$data['list_data'] = $this->query_data_pemasukan($this->request);
			}else{
				$data['list_data'] = $this->query_data_pengeluaran($this->request);
			}
		}

		return view('laporan/harian',$data);
	}

	protected function query_data_pemasukan($request){
		$db      = \Config\Database::connect();

		$bulan = $request->getGet("bulan");
		$tahun = $request->getGet("tahun");

		$data_kelompok = array();
		$data = array();

		$query_kelompok = $db->table("master_kelompok")->get()->getResult();
		foreach($query_kelompok AS $v){
			$data_kelompok[$v->kelompok_id] = $v->nama_kelompok;
		}

		$querydata = $db->table("transaksi")
			->select("
				SUM(simpanan_pokok) AS simpanan_pokok,
				SUM(simpanan_wajib) AS simpanan_wajib,
				SUM(simpanan_sukarela) AS simpanan_sukarela,
				SUM(cicilan_pinjaman) AS cicilan_pinjaman,
				transaksi.no_anggota,
				nama_anggota,
				kelompok,
				SUM(jasa_dibayar) AS jasa_dibayar"
			)
			->join('master_anggota', 'transaksi.no_anggota = master_anggota.no_anggota','inner')
			->join('transaksi_pemasukan', 'transaksi.transaksi_id = transaksi_pemasukan.transaksi_id','inner')
			->where('MONTH(tanggal_transaksi)',$bulan)
			->where('YEAR(tanggal_transaksi)',$tahun)
			->groupBy(array("transaksi.no_anggota","nama_anggota","kelompok"))
			->orderBy("transaksi.no_anggota")
			->get()->getResult();
		
		foreach($querydata AS $v2){
			//if($total > 0){
				
				if(isset($data_kelompok[$v2->kelompok])){
					$data[$data_kelompok[$v2->kelompok]][] = array(
						'nama_anggota' => $v2->nama_anggota,
						'no_anggota' => $v2->no_anggota,
						'simpanan_pokok' => $v2->simpanan_pokok,
						'simpanan_wajib' => $v2->simpanan_wajib,
						'simpanan_sukarela' => $v2->simpanan_sukarela,
						'cicilan_pinjaman' => $v2->cicilan_pinjaman,
						'jasa_dibayar' => $v2->jasa_dibayar,
					);
				}				
			//}				
		}
		return $data;
	}

	protected function query_data_pengeluaran($request){
		$db      = \Config\Database::connect();

		$bulan = $request->getGet("bulan");
		$tahun = $request->getGet("tahun");

		$data_kelompok = array();
		$data = array();

		$query_kelompok = $db->table("master_kelompok")->get()->getResult();
		foreach($query_kelompok AS $v){
			$data_kelompok[$v->kelompok_id] = $v->nama_kelompok;
		}

		$querydata = $db->table("transaksi")
			->select("SUM(simpanan_pokok) AS simpanan_pokok,SUM(simpanan_wajib) AS simpanan_wajib,SUM(simpanan_sukarela) AS simpanan_sukarela,SUM(pinjaman) AS pinjaman,transaksi.no_anggota,nama_anggota,kelompok")
			->join('master_anggota', 'transaksi.no_anggota = master_anggota.no_anggota','inner')
			->join('transaksi_pengeluaran', 'transaksi.transaksi_id = transaksi_pengeluaran.transaksi_id','left')
			->join('transaksi_pemasukan', 'transaksi.transaksi_id = transaksi_pemasukan.transaksi_id','left')
			->where('MONTH(tanggal_transaksi)',$bulan)
			->where('YEAR(tanggal_transaksi)',$tahun)
			->groupBy("transaksi.no_anggota")
			->orderBy("transaksi.no_anggota")
			->get()->getResult();

		foreach($querydata AS $v2){
			if(isset($data_kelompok[$v2->kelompok])){
				$data[$data_kelompok[$v2->kelompok]][] = array(
					'nama_anggota' => $v2->nama_anggota,
					'no_anggota' => $v2->no_anggota,
					'simpanan_pokok' => $v2->simpanan_pokok,
					'simpanan_wajib' => $v2->simpanan_wajib,
					'simpanan_sukarela' => $v2->simpanan_sukarela,
					'pinjaman' => $v2->pinjaman,
				);
			}				
		}

		return $data;
	}
}
