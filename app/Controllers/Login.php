<?php

namespace App\Controllers;
use App\Models\UserModel;

class Login extends BaseController
{
	
	public function index()
	{
		if($this->request->getVar("login")){
			return $this->auth_login();
		}
		return view('login/index');
	}

	private function auth_login(){
		$session = session();
        $model = new UserModel();
        $username = $this->request->getVar('username');
	    $password = $this->request->getVar('password');
        $data = $model->where('username', $username)->first();
        if($data){
            $pass = $data['password'];
            $verify_pass = password_verify($password, $pass);
           
		    if($verify_pass){
                $ses_data = [
                    'user_id'       => $data['user_id'],
                    'username'     => $data['username'],
                    'group_id'    => $data['group_id'],
                    'logged_in'     => TRUE
                ];
                $session->set($ses_data);
                return redirect()->to('/home');
            }else{
                $session->setFlashdata('status_failed', 'Wrong Password');
                return redirect()->to('/login');
            }
        }else{
            $session->setFlashdata('status_failed', 'Email not Found');
            return redirect()->to('/login');
        }
	}



}
