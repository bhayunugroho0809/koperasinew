<?php

namespace App\Controllers;
use App\Models\AnggotaModel;
use App\Models\DinasModel;
use App\Models\KecamatanModel;
use App\Models\KelompokModel;
use Config\Services;
class Master_anggota extends BaseController
{

	public function index()
	{
        $model = new AnggotaModel();
        $modelkecamatan = new KecamatanModel();
        $modelkelompok = new KelompokModel();
        $modeldinas = new DinasModel();
        $data = [
            'data_list' => $model->orderBy('no_anggota')->paginate(10,'bootstrap'),
            'pager' => $model->pager,
            'kecamatan' => $modelkecamatan->findAll(),
            'kelompok' => $modelkelompok->findAll(),
            'dinas' => $modeldinas->findAll()
        ];
        
        if($this->request->getPost("simpan")){
            return $this->save_anggota(); 
        }
        return view('master/anggota/index',$data);
	}

    public function list_anggota(){
        $model = new AnggotaModel();
        $data = [
            'data' => $model->paginate(10),
            'pager' => $model->pager,
        ];
      
        return $data;
    }

    public function list_select_anggota(){
        $model = new AnggotaModel();
        

        if(!empty($this->request->getGet("term"))){
			$data_anggota = $model->where("nama_anggota","like","%".$this->request->getGet("term")."%")->findAll();
		}else{
			$data_anggota = $model->findAll();
		}
        
		$data = array();		
		foreach($data_anggota as $f){
			$data[]=array(
		        'id'=>$f['no_anggota'],
			    'text'=>$f['no_anggota'].' - '.$f['nama_anggota'],
			);
		}
        
        return json_encode($data);
    }

    public function print(){
        $model = new AnggotaModel();
        $anggota_id  = $this->request->getGet("id");
        
        $data_anggota = $model->where('anggota_id', $anggota_id)->first();
        $data["anggota"] = $data_anggota;
        return view('master/anggota/print',$data);
    }
    public function anggota_detail(){
        $no_anggota  = $this->request->getGet("no_anggota");
        $model = new AnggotaModel();
        $data = $model->where('no_anggota', $no_anggota)
            ->join('master_kelompok','master_anggota.kelompok=master_kelompok.kelompok_id','left')
            ->first();
        if($data){
            return json_encode(array("success"=>true ,"data"=>$data));
        }else{
            return json_encode(array("success"=>false,"data"=>null));;;
        }
    }
    public function delete($id){
        $session = session();
        $model = new AnggotaModel();
        if($model->where('anggota_id', $id)->delete()){
            $session->setFlashdata("message_success","Data Berhasil dihapus");
            return redirect()->to('/master_anggota');
        }else{
            $session->setFlashdata("message_failed","Data gagal Dihapus");
            return redirect()->to('/master_anggota');
        }
    }


    public function detail(){
        $session = session();
        $model = new AnggotaModel();
        $id = $this->request->getGet("anggota_id");
        $data = $model->where('anggota_id', $id)->first();

        $data['tanggal_masuk'] = date_indo($data['tanggal_masuk']);
        $data['tanggal_lahir'] = date_indo($data['tanggal_lahir']);
       
        return json_encode(array("success"=>true,"data"=>$data));
    }

    protected function save_anggota(){
        $session = session();
        $model = new AnggotaModel();

        if (!$this->validate([
            'nama_anggota' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Nama Anggota Harus diisi'
                ]
            ],
            'no_anggota' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'No Anggota Harus diisi'
                ]
            ],
            'tanggal_masuk' => [
                'rules' => 'required',
                'errors' => [
                'required' => 'Tanggal Masuk Harus diisi'
                ]
            ]
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        } else {
            $data = [
                'nik_anggota'     => $this->request->getPost('nik_anggota'),
                'nama_anggota'     => $this->request->getPost('nama_anggota'),
                'no_anggota'     => $this->request->getPost('no_anggota'),
                'tanggal_masuk'     => date_database($this->request->getPost('tanggal_masuk')),
                'rt'     => $this->request->getPost('rt'),
                'rw'     => $this->request->getPost('rw'),
                'agama'     => $this->request->getPost('agama'),
                'pendidikan'     => $this->request->getPost('pendidikan'),
                'pekerjaan'     => $this->request->getPost('pekerjaan'),
                'telepon1'     => $this->request->getPost('telepon1'),
                'telepon2'     => $this->request->getPost('telepon2'),
                'alamat'     => $this->request->getPost('alamat'),
                'kecamatan'     => $this->request->getPost('kecamatan'),
                'kelompok'     => $this->request->getPost('kelompok'),
                'kode_pos'     => $this->request->getPost('kode_pos'),
                'dinas'     => $this->request->getPost('dinas'),
                'tempat_lahir'     => $this->request->getPost('tempat_lahir'),
                'tanggal_lahir'     => date_database($this->request->getPost('tanggal_lahir')),
                'jenis_kelamin'     => $this->request->getPost('jenis_kelamin'),
                'nama_penjamin'     => $this->request->getPost('nama_penjamin'),
                'data_penjamin'     => $this->request->getPost('data_penjamin'),
                'agama_penjamin'     => $this->request->getPost('agama_penjamin'),
                'nik_penjamin'     => $this->request->getPost('nik_penjamin'),
                'alamat_penjamin'     => $this->request->getPost('alamat_penjamin'),
                'telepon_penjamin'     => $this->request->getPost('telepon_penjamin'),
                'pendidikan_penjamin'     => $this->request->getPost('pendidikan_penjamin'),
                'pekerjaan_penjamin'     => $this->request->getPost('pekerjaan_penjamin'),
                'alamat_kantor_penjamin'     => $this->request->getPost('alamat_kantor_penjamin'),
                'status'     => $this->request->getPost('status'),
                'created_at'    => date("Y-m-d H:i:s")
            ];
            if($model->save($data)){
                $session->setFlashdata("message_success","Data Berhasil Disimpan");
                return redirect()->to('/master_anggota');
            }else{
                $session->setFlashdata("message_failed","Data gagal Disimpan");
                return redirect()->to('/master_anggota');
            }
        }
      
    }

    public function update(){
        $session = session();
        $model = new AnggotaModel();
        $id = $this->request->getPost("anggota_id");
        $data = [
            'nik_anggota'     => $this->request->getPost('nik_anggota'),
            'nama_anggota'     => $this->request->getPost('nama_anggota'),
            'no_anggota'     => $this->request->getPost('no_anggota'),
            'tanggal_masuk'     => date_database($this->request->getPost('tanggal_masuk')),
            'rt'     => $this->request->getPost('rt'),
            'rw'     => $this->request->getPost('rw'),
            'agama'     => $this->request->getPost('agama'),
            'pendidikan'     => $this->request->getPost('pendidikan'),
            'pekerjaan'     => $this->request->getPost('pekerjaan'),
            'telepon1'     => $this->request->getPost('telepon1'),
            'telepon2'     => $this->request->getPost('telepon2'),
            'alamat'     => $this->request->getPost('alamat'),
            'kecamatan'     => $this->request->getPost('kecamatan'),
            'kelompok'     => $this->request->getPost('kelompok'),
            'kode_pos'     => $this->request->getPost('kode_pos'),
            'dinas'     => $this->request->getPost('dinas'),
            'tempat_lahir'     => $this->request->getPost('tempat_lahir'),
            'tanggal_lahir'     => date_database($this->request->getPost('tanggal_lahir')),
            'jenis_kelamin'     => $this->request->getPost('jenis_kelamin'),
            'nama_penjamin'     => $this->request->getPost('nama_penjamin'),
            'data_penjamin'     => $this->request->getPost('data_penjamin'),
            'agama_penjamin'     => $this->request->getPost('agama_penjamin'),
            'nik_penjamin'     => $this->request->getPost('nik_penjamin'),
            'alamat_penjamin'     => $this->request->getPost('alamat_penjamin'),
            'telepon_penjamin'     => $this->request->getPost('telepon_penjamin'),
            'pendidikan_penjamin'     => $this->request->getPost('pendidikan_penjamin'),
            'pekerjaan_penjamin'     => $this->request->getPost('pekerjaan_penjamin'),
            'alamat_kantor_penjamin'     => $this->request->getPost('alamat_kantor_penjamin'),
            'status'     => $this->request->getPost('status'),
            'updated_at'    => date("Y-m-d H:i:s")
        ];
        $update = $model->where("anggota_id",$id)->set($data)->update();
        if($update){
            $session->setFlashdata("message_success","Data Berhasil Diubah");
            return redirect()->to('/master_anggota');
        }else{
            $session->setFlashdata("message_failed","Data gagal Diubah");
            return redirect()->to('/master_anggota');
        }
    }
}
