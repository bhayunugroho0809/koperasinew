<?php

namespace App\Controllers;
use App\Models\DinasModel;
use Config\Services;
class Master_dinas extends BaseController
{
	public function index()
	{
        $model = new DinasModel();
        $data = [
            'data_list' => $model->paginate(10,'bootstrap'),
            'pager' => $model->pager,
        ];
        if($this->request->getPost("simpan")){
            return $this->save_dinas(); 
        }
        return view('master/dinas/index',$data);
	}

    public function list_dinas(){
        $model = new DinasModel();
        $data = [
            'data' => $model->paginate(10),
            'pager' => $model->pager,
        ];
        return $data;
    }


    public function delete($id){
        $session = session();
        $model = new DinasModel();
        if($model->where('dinas_id', $id)->delete()){
            $session->setFlashdata("message_success","Data Berhasil dihapus");
            return redirect()->to('/master_dinas');
        }else{
            $session->setFlashdata("message_failed","Data gagal Dihapus");
            return redirect()->to('/master_dinas');
        }
    }


    public function detail(){
        $session = session();
        $model = new DinasModel();
        $id = $this->request->getGet("dinas_id");
        $data = $model->where('dinas_id', $id)->first();
       
        return json_encode($data);
    }

    protected function save_dinas(){
        $session = session();
        $model = new DinasModel();
        $data = [
            'nama_dinas'     => $this->request->getPost('nama_dinas'),
            'created_at'    => date("Y-m-d H:i:s")
        ];
        if($model->save($data)){
            $session->setFlashdata("message_success","Data Berhasil Disimpan");
            return redirect()->to('/master_dinas');
        }else{
            $session->setFlashdata("message_failed","Data gagal Disimpan");
            return redirect()->to('/master_dinas');
        }
    }

    public function update(){
        $session = session();
        $model = new DinasModel();
        $data = [
            'nama_dinas'     => $this->request->getPost('nama_dinas'),
            'updated_at'    => date("Y-m-d H:i:s")
        ];
        $id = $this->request->getPost("dinas_id");
        $update = $model->where("dinas_id",$id)->set(["nama_dinas"=>$this->request->getPost("nama_dinas"),"updated_at"=>date("Y-m-d H:i:s")])->update( );
        if($update){
            $session->setFlashdata("message_success","Data Berhasil Diubah");
            return redirect()->to('/master_dinas');
        }else{
            $session->setFlashdata("message_failed","Data gagal Diubah");
            return redirect()->to('/master_dinas');
        }
    }
}
