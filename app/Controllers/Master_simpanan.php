<?php

namespace App\Controllers;
use App\Models\SimpananModel;
use Config\Services;
class Master_simpanan extends BaseController
{
	public function index()
	{
        $model = new SimpananModel();
        $data = [
            'data_list' => $model->paginate(10,'bootstrap'),
            'pager' => $model->pager,
        ];
        if($this->request->getPost("simpan")){
            return $this->save_simpanan(); 
        }
        return view('master/simpanan/index',$data);
	}

    public function list_simpanan(){
        $model = new SimpananModel();
        $data = [
            'data' => $model->paginate(10),
            'pager' => $model->pager,
        ];
        return $data;
    }

    public function delete($id){
        $session = session();
        $model = new SimpananModel();
        if($model->where('simpanan_id', $id)->delete()){
            $session->setFlashdata("message_success","Data Berhasil dihapus");
            return redirect()->to('/master_simpanan');
        }else{
            $session->setFlashdata("message_failed","Data gagal Dihapus");
            return redirect()->to('/master_simpanan');
        }
    }


    public function detail(){
        $session = session();
        $model = new SimpananModel();
        $id = $this->request->getGet("simpanan_id");
        $data = $model->where('simpanan_id', $id)->first();
       
        return json_encode($data);
    }

    protected function save_simpanan(){
        $session = session();
        $model = new SimpananModel();
        $data = [
            'nama_simpanan'     => $this->request->getPost('nama_simpanan'),
            'jumlah'     => $this->request->getPost('jumlah'),
            'created_at'    => date("Y-m-d H:i:s")
        ];
        if($model->save($data)){
            $session->setFlashdata("message_success","Data Berhasil Disimpan");
            return redirect()->to('/master_simpanan');
        }else{
            $session->setFlashdata("message_failed","Data gagal Disimpan");
            return redirect()->to('/master_simpanan');
        }
    } 

    public function update(){
        $session = session();
        $model = new SimpananModel();
        $data = [
            'nama_simpanan'     => $this->request->getPost('nama_simpanan'),
            'jumlah'     => $this->request->getPost('jumlah'),
            'updated_at'    => date("Y-m-d H:i:s")
        ];
        $id = $this->request->getPost("simpanan_id");
        $update = $model->where("simpanan_id",$id)->set($data)->update();
        if($update){
            $session->setFlashdata("message_success","Data Berhasil Diubah");
            return redirect()->to('/master_simpanan');
        }else{
            $session->setFlashdata("message_failed","Data gagal Diubah");
            return redirect()->to('/master_simpanan');
        }
    }
}
