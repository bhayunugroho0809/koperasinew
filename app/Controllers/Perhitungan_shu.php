<?php

namespace App\Controllers;

use Exception;

class Perhitungan_shu extends BaseController
{
	public function index()
	{
		$data['list_data'] = [];
		if($this->request){
			$data['list_data'] = $this->query_data_nominatif($this->request);
		}

		return view('laporan/shu',$data);
	}

	protected function query_data_nominatif($request){
		$db      = \Config\Database::connect();

		$bulan = $request->getGet("bulan");
		$tahun = $request->getGet("tahun");

		$data_kelompok = array();
		$data = array();

		$query_kelompok = $db->table("master_kelompok")->get()->getResult();
		foreach($query_kelompok AS $v){
			$data_kelompok[$v->kelompok_id] = $v->nama_kelompok;
		}

		$querydata = $db->table("transaksi")
			->select("
				SUM(simpanan_pokok) AS simpanan_pokok,
				SUM(simpanan_wajib) AS simpanan_wajib,
				SUM(simpanan_sukarela) AS simpanan_sukarela,
				SUM(pengambilan_pokok) AS pengambilan_pokok,
				SUM(pengambilan_wajib) AS pengambilan_wajib,
				SUM(pengambilan_sukarela) AS pengambilan_sukarela,
				SUM(cicilan_pinjaman) AS cicilan_pinjaman,
				SUM(pinjaman) AS pinjaman,
				transaksi.no_anggota,nama_anggota,
				kelompok,
				SUM(jasa_dibayar) AS jasa_dibayar,
				SUM(jasa) AS jasa")
			->join('master_anggota', 'transaksi.no_anggota = master_anggota.no_anggota','inner')
			->join('transaksi_pemasukan', 'transaksi.transaksi_id = transaksi_pemasukan.transaksi_id','left')
			->join('transaksi_pengeluaran', 'transaksi.transaksi_id = transaksi_pemasukan.transaksi_id','left')
			->where('YEAR(tanggal_transaksi) >=','2018')
			//->where('YEAR(tanggal_transaksi) <=',$tahun)
			->groupBy("transaksi.no_anggota")
			->orderBy("transaksi.no_anggota")
			->get()->getResult();
		
		$sisa_pokok = 0;
		$sisa_wajib = 0;
		$sisa_sukarela = 0;
		$pinjaman = [];		
			
		foreach($querydata AS $v2){
			//if($total > 0){
				$sisa_pokok = ($v2->simpanan_pokok - $v2->pengambilan_pokok);
				$sisa_wajib = ($v2->simpanan_wajib - $v2->pengambilan_wajib);
				$sisa_sukarela = ($v2->simpanan_sukarela - $v2->pengambilan_sukarela);
				$total = $sisa_pokok + $sisa_sukarela + $sisa_wajib;
				$pinjaman[$data_kelompok[$v->kelompok_id]][] = $v2->pinjaman - $v2->cicilan_pinjaman;		
				
				if(isset($data_kelompok[$v2->kelompok])){
					$data[$data_kelompok[$v2->kelompok]][] = array(
						'nama_anggota' => $v2->nama_anggota,
						'no_anggota' => $v2->no_anggota,
						'simpanan_pokok' => $sisa_pokok,
						'simpanan_wajib' => $sisa_wajib,
						'simpanan_sukarela' => $sisa_sukarela,
						'jumlah_simpanan' => $total,
						'pokok' => $v2->pinjaman,
						'jumlah_bayar' => $v2->cicilan_pinjaman,
						'sisa' => array_sum($pinjaman[$data_kelompok[$v2->kelompok]]),
						'sisa_jasa' => (isset($sisa_jasa) ? $sisa_jasa : 0 ),
						'jasa_dibayar' => $v2->jasa,
					);
				}				
			//}				
		}
		return $data;
	}

}
