<?php

namespace App\Controllers;
use App\Models\KelompokModel;
use Config\Services;
class Master_kelompok extends BaseController
{
	public function index()
	{
        $model = new KelompokModel();
        $data = [
            'data_list' => $model->paginate(10,'bootstrap'),
            'pager' => $model->pager,
        ];
        if($this->request->getPost("simpan")){
            return $this->save_kelompok(); 
        } 
        return view('master/kelompok/index',$data);
	}

    public function list_kelompok(){
        $model = new KelompokModel();
        $data = [
            'data' => $model->paginate(10),
            'pager' => $model->pager,
        ];
        return $data;
    }

    public function delete($id){
        $session = session();
        $model = new KelompokModel();
        if($model->where('kelompok_id', $id)->delete()){
            $session->setFlashdata("message_success","Data Berhasil dihapus");
            return redirect()->to('/master_kelompok');
        }else{
            $session->setFlashdata("message_failed","Data gagal Dihapus");
            return redirect()->to('/master_kelompok');
        }
    }


    public function kelompok_detail(){
        $session = session();
        $model = new KelompokModel();
        $id = $this->request->getGet("kelompok_id");
        $data = $model->where('kelompok_id', $id)->first();
       
        return json_encode($data);
    }

    protected function save_kelompok(){
        $session = session();
        $model = new KelompokModel();
        $data = [
            'nama_kelompok'     => $this->request->getPost('nama_kelompok'),
            'created_at'    => date("Y-m-d H:i:s")
        ];
        if($model->save($data)){
            $session->setFlashdata("message_success","Data Berhasil Disimpan");
            return redirect()->to('/master_kelompok');
        }else{
            $session->setFlashdata("message_failed","Data gagal Disimpan");
            return redirect()->to('/master_kelompok');
        }
    }

    public function kelompok_update(){
        $session = session();
        $model = new KelompokModel();
        $data = [
            'nama_kelompok'     => $this->request->getPost('nama_kelompok'),
            'updated_at'    => date("Y-m-d H:i:s")
        ];
        $id = $this->request->getPost("kelompok_id");
        $update = $model->where("kelompok_id",$id)->set(["nama_kelompok"=>$this->request->getPost("nama_kelompok"),"updated_at"=>date("Y-m-d H:i:s")])->update( );
        if($update){
            $session->setFlashdata("message_success","Data Berhasil Diubah");
            return redirect()->to('/master_kelompok');
        }else{
            $session->setFlashdata("message_failed","Data gagal Diubah");
            return redirect()->to('/master_kelompok');
        }
    }
}
