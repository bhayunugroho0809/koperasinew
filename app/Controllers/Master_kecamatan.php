<?php

namespace App\Controllers;
use App\Models\KecamatanModel;
use Config\Services;
class Master_kecamatan extends BaseController
{
	public function index()
	{
        $model = new KecamatanModel();
        $data = [
            'data_list' => $model->paginate(10,'bootstrap'),
            'pager' => $model->pager,
        ];
        if($this->request->getPost("simpan")){
            return $this->save_kecamatan(); 
        }
        return view('master/kecamatan/index',$data);
	}
    

    public function list_kecamatan(){
        $model = new KecamatanModel();
        $data = [
            'data' => $model->paginate(10),
            'pager' => $model->pager,
        ];
        return $data;
    }

    public function delete($id){
        $session = session();
        $model = new KecamatanModel();
        if($model->where('kecamatan_id', $id)->delete()){
            $session->setFlashdata("message_success","Data Berhasil dihapus");
            return redirect()->to('/master_kecamatan');
        }else{
            $session->setFlashdata("message_failed","Data gagal Dihapus");
            return redirect()->to('/master_kecamatan');
        }
    }


    public function kecamatan_detail(){
        $session = session();
        $model = new KecamatanModel();
        $id = $this->request->getGet("kecamatan_id");
        $data = $model->where('kecamatan_id', $id)->first();
       
        return json_encode($data);
    }

    protected function save_kecamatan(){
        $session = session();
        $model = new KecamatanModel();
        $data = [
            'nama_kecamatan'     => $this->request->getPost('nama_kecamatan'),
            'created_at'    => date("Y-m-d H:i:s")
        ];
        if($model->save($data)){
            $session->setFlashdata("message_success","Data Berhasil Disimpan");
            return redirect()->to('/master_kecamatan');
        }else{
            $session->setFlashdata("message_failed","Data gagal Disimpan");
            return redirect()->to('/master_kecamatan');
        }
    }

    public function kecamatan_update(){
        $session = session();
        $model = new KecamatanModel();
        $data = [
            'nama_kecamatan'     => $this->request->getPost('nama_kecamatan'),
            'updated_at'    => date("Y-m-d H:i:s")
        ];
        $id = $this->request->getPost("kecamatan_id");
        $update = $model->where("kecamatan_id",$id)->set(["nama_kecamatan"=>$this->request->getPost("nama_kecamatan"),"updated_at"=>date("Y-m-d H:i:s")])->update( );
        if($update){
            $session->setFlashdata("message_success","Data Berhasil Diubah");
            return redirect()->to('/master_kecamatan');
        }else{
            $session->setFlashdata("message_failed","Data gagal Diubah");
            return redirect()->to('/master_kecamatan');
        }
    }
}
