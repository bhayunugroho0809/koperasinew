<?php

namespace App\Controllers;

use Exception;

class Laporan_ringkas_bulanan extends BaseController
{
	public function index()
	{
		$data['list_data'] = [];
		if($this->request){
			if($this->request->getGet("jenis_laporan") == "1"){
				$data['list_data'] = $this->query_data_pemasukan($this->request);
			}else{
				$data['list_data'] = $this->query_data_pengeluaran($this->request);
			}
		}

		return view('laporan/ringkas_bulanan',$data);
	}

	protected function query_data_pemasukan($request){
		$db      = \Config\Database::connect();

		$bulan = $request->getGet("bulan");
		$tahun = $request->getGet("tahun");

		$days=null;
		try{
			$days=cal_days_in_month(CAL_GREGORIAN,(int)$bulan,$tahun);
		}catch(Exception $e){

		}
		$tanggal_awal = $tahun."-".$bulan."-01";
		$tanggal_akhir = $tahun."-".$bulan."-".$days;

		$querydata = $db->table("transaksi")
			->select("SUM(simpanan_pokok) AS simpanan_pokok,SUM(simpanan_wajib) AS simpanan_wajib,SUM(simpanan_sukarela) AS simpanan_sukarela,SUM(cicilan_pinjaman) AS cicilan_pinjaman,tanggal_transaksi,SUM(jasa_dibayar) AS jasa_dibayar")
			->join('transaksi_pemasukan', 'transaksi.transaksi_id = transaksi_pemasukan.transaksi_id','inner')
			->where('tanggal_transaksi >=',$tanggal_awal)
			->where('tanggal_transaksi <=',$tanggal_akhir)
			->groupBy("tanggal_transaksi")
			->orderBy("tanggal_transaksi")
			->get()->getResult();

		$query_simpanan_wajib = [];
		$query_simpanan_pokok = [];
		$query_simpanan_sukarela = [];
		$query_cicilan_pinjaman = [];
		$query_jasa_dibayar = [];

		foreach($querydata AS $k=>$v){
			$query_simpanan_wajib[$v->tanggal_transaksi] = $v->simpanan_wajib;	
			$query_simpanan_pokok[$v->tanggal_transaksi] = $v->simpanan_pokok;	
			$query_simpanan_sukarela[$v->tanggal_transaksi] = $v->simpanan_sukarela;	
			$query_cicilan_pinjaman[$v->tanggal_transaksi] = $v->cicilan_pinjaman;	
			$query_jasa_dibayar[$v->tanggal_transaksi] = $v->jasa_dibayar;	
		}
		
		$data = [];
		if($days > 0){
			for($i=1;$i<=$days;$i++){
				$day = (strlen($i) > 1 ? $i : "0".$i);
				$tgl = $tahun."-".$bulan."-".$day;
				$data[] = [
					"hari"=>$i,
					"simpanan_pokok" => (!empty($query_simpanan_pokok[$tgl]) ? $query_simpanan_pokok[$tgl] : 0 ),
					"simpanan_wajib" => (!empty($query_simpanan_wajib[$tgl]) ? $query_simpanan_wajib[$tgl] : 0 ),
					"simpanan_sukarela" => (!empty($query_simpanan_sukarela[$tgl]) ? $query_simpanan_sukarela[$tgl] : 0 ),
					"cicilan_pinjaman" => (!empty($query_cicilan_pinjaman[$tgl]) ? $query_cicilan_pinjaman[$tgl] : 0 ),
					"jasa_dibayar" => (!empty($query_jasa_dibayar[$tgl]) ? $query_jasa_dibayar[$tgl] : 0 ),
				];
			}
		}
	
		return $data;
	}

	protected function query_data_pengeluaran($request){
		$db      = \Config\Database::connect();

		$bulan = $request->getGet("bulan");
		$tahun = $request->getGet("tahun");

		$days=null;
		try{
			$days=cal_days_in_month(CAL_GREGORIAN,(int)$bulan,$tahun);
		}catch(Exception $e){

		}
		$tanggal_awal = $tahun."-".$bulan."-01";
		$tanggal_akhir = $tahun."-".$bulan."-".$days;

		$querydata = $db->table("transaksi")
			->select("SUM(pengambilan_pokok) AS pengambilan_pokok,SUM(pengambilan_wajib) AS pengambilan_wajib,SUM(pengambilan_sukarela) AS pengambilan_sukarela,SUM(pinjaman) AS pinjaman,tanggal_transaksi,SUM(jasa) AS jasa")
			->join('transaksi_pengeluaran', 'transaksi.transaksi_id = transaksi_pengeluaran.transaksi_id','inner')
			->where('tanggal_transaksi >=',$tanggal_awal)
			->where('tanggal_transaksi <=',$tanggal_akhir)
			->groupBy("tanggal_transaksi")
			->orderBy("tanggal_transaksi")
			->get()->getResult();

		$query_pengambilan_wajib = [];
		$query_pengambilan_pokok = [];
		$query_pengambilan_sukarela = [];
		$query_pinjaman = [];
		$query_jasa = [];

		foreach($querydata AS $k=>$v){
			$query_pengambilan_wajib[$v->tanggal_transaksi] = $v->pengambilan_wajib;	
			$query_pengambilan_pokok[$v->tanggal_transaksi] = $v->pengambilan_pokok;	
			$query_pengambilan_sukarela[$v->tanggal_transaksi] = $v->pengambilan_sukarela;	
			$query_pinjaman[$v->tanggal_transaksi] = $v->pinjaman;	
			$query_jasa[$v->tanggal_transaksi] = $v->query_jasa;	
		}
		
		$data = [];
		if($days > 0){
			for($i=1;$i<=$days;$i++){
				$day = (strlen($i) > 1 ? $i : "0".$i);
				$tgl = $tahun."-".$bulan."-".$day;
				$data[] = [
					"hari"=>$i,
					"pengambilan_pokok" => (!empty($query_pengambilan_pokok[$tgl]) ? $query_pengambilan_pokok[$tgl] : 0 ),
					"pengambilan_wajib" => (!empty($query_pengambilan_wajib[$tgl]) ? $query_pengambilan_wajib[$tgl] : 0 ),
					"pengambilan_sukarela" => (!empty($query_pengambilan_sukarela[$tgl]) ? $query_pengambilan_sukarela[$tgl] : 0 ),
					"pinjaman" => (!empty($query_pinjaman[$tgl]) ? $query_pinjaman[$tgl] : 0 ),
					"jasa" => (!empty($query_jasa[$tgl]) ? $query_jasa[$tgl] : 0 ),
				];
			}
		}
	
		return $data;
	}
}
