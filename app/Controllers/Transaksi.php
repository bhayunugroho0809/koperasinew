<?php

namespace App\Controllers;

use App\Models\AnggotaModel;
use App\Models\LogMasterModel;
use App\Models\TransaksiModel;
use App\Models\TransaksiPemasukanModel;
use App\Models\TransaksiPengeluaranModel;
use App\Models\TransaksiPinjamModel;
use Config\Services;
class Transaksi extends BaseController
{
	public function index()
	{
        $db      = \Config\Database::connect();
        $builder = $db->table('transaksi'); 
        $builder->selectCount('transaksi_id');
        $count_transaksi = ($builder->countAll() + 1);

        $session = session();
        $model = new TransaksiModel();
        $bio = new AnggotaModel();
        
        $get_bio = null;
        
        $data = [
            'data_list' => $model->paginate(10,'bootstrap'),
            'pager' => $model->pager,
            'count_transaksi' => $count_transaksi,
            'get_bio' => $get_bio,
            'list_anggota' => $bio->findAll(),
        ];
        return view('transaksi/index',$data);
	}


    public function list_simpanan(){
        $no_anggota = $this->request->getGet("no_anggota");

        if(!$no_anggota){
            return json_encode(array("success"=>false,"info"=>"No Anggota Wajib diisi","data"=>null));
        }
        $db      = \Config\Database::connect();
       
        $model = new TransaksiModel();    
        $data = $db->table('transaksi')
            ->select("tanggal_transaksi,kode_transaksi,simpanan_pokok,simpanan_sukarela,simpanan_wajib")
            ->join('transaksi_pemasukan','transaksi.transaksi_id=transaksi_pemasukan.transaksi_id')
            ->where(array("no_anggota"=>$no_anggota))
            ->whereIn("kode_transaksi",array("01","02","03"))
            ->orderBy("tanggal_transaksi")
            ->get()->getResultArray();  

        $datasum = $db->table('transaksi')
            ->select("SUM(simpanan_pokok) AS jml_simpanan_pokok,SUM(simpanan_sukarela) AS jml_simpanan_sukarela,SUM(simpanan_wajib) AS jml_simpanan_wajib,kode_transaksi")
            ->join('transaksi_pemasukan','transaksi.transaksi_id=transaksi_pemasukan.transaksi_id')
            ->where(array("no_anggota"=>$no_anggota))
            ->whereIn("kode_transaksi",array("01","02","03"))
            ->groupBy("kode_transaksi")
            ->orderBy("kode_transaksi")
            ->get()->getResultArray();  
        if(!$data){
            return json_encode(array("success"=>true,"info"=>"Data Tidak tersedia","data"=>null,"datasum"=>$datasum));
        }

        return json_encode(array("success"=>true,"info"=>"Data Sukses","data"=>$data));
    }

    public function kunci_proses(){
        $session = session();
        $model = new LogMasterModel();    
        $data_save = [
            "bulan" => $this->request->getPost("bulan"),
            "tahun" => $this->request->getPost("tahun"),
            "keterangan" => $this->request->getPost("keterangan"),
            "previlege_insert" => (!empty($this->request->getPost("previlege_insert")) ? $this->request->getPost("previlege_insert") : "N"),
            "previlege_update" => (!empty($this->request->getPost("previlege_update")) ? $this->request->getPost("previlege_update") : "N"),
            "previlege_delete" => (!empty($this->request->getPost("previlege_delete")) ? $this->request->getPost("previlege_delete") : "N"),
            "created_at" => date("Y-m-d H:i:s")
        ];
        
        if($model->save($data_save)){
            $session->setFlashdata("message_success","Data Berhasil Di Kunci");
            return redirect()->back();
        }else{
            $session->setFlashdata("message_failed","Data gagal Dikunci");
            return redirect()->back();
        }
    }

    public function list_pinjaman(){
        $no_anggota = $this->request->getGet("no_anggota");

        if(!$no_anggota){
            return json_encode(array("success"=>false,"info"=>"No Anggota Wajib diisi","data"=>null));
        }
        $db      = \Config\Database::connect();
       
        $data = $db->table('transaksi')
        ->select("tanggal_transaksi,
            CASE kode_transaksi
                when '04' then SUM(IFNULL(pinjaman,0)) 
                when '06' then SUM(IFNULL(jasa,0)) 
            END AS debit,
            CASE kode_transaksi
                when '05' then SUM(IFNULL(cicilan_pinjaman,0)) 
                when '07' then SUM(IFNULL(jasa_dibayar,0)) 
                when '08' then SUM(IFNULL(pengambilan_sukarela,0))
            END AS kredit,
            kode_transaksi")
        ->join('transaksi_pemasukan','transaksi.transaksi_id=transaksi_pemasukan.transaksi_id','left')
        ->join('transaksi_pengeluaran','transaksi.transaksi_id=transaksi_pengeluaran.transaksi_id','left')
        ->where(array("no_anggota"=>$no_anggota))
        ->whereIn("kode_transaksi",array("04","05","06","07","08"))
        ->groupBy(array("tanggal_transaksi","kode_transaksi"))
        ->get()->getResultArray();  

        if(!$data){
            return json_encode(array("success"=>true,"info"=>"Data Tidak tersedia","data"=>null));
        }

        return json_encode(array("success"=>true,"info"=>"Data Sukses","data"=>$data));
    }

    public function list_log(){
        $session = session();
        $model = new LogMasterModel();

        $data = $model->findAll();
        if(!$data){
            return json_encode(array("success"=>true,"info"=>"Data Tidak tersedia","data"=>null));
        }

        return json_encode(array("success"=>true,"info"=>"Data Sukses","data"=>$data));
    }

    public function save_form(){
       
        $kode_transaksi = $this->request->getPost("kode_transaksi");
        list($tgl,$bln,$thn) = explode("/",$this->request->getPost('tanggal_transaksi'));
        $model_log = new LogMasterModel();
        $data = $model_log->where(["bulan"=>$bln,"tahun"=>$thn])->first();

        if(!empty($data)){
            return json_encode(array("success"=>false,"info"=>"Data untuk Bulan ".$bln." ".$thn." telah di Kunci" ,"data"=>null));
            exit();
        }

        
        $tanggal_transaksi = $thn."-".$bln."-".$tgl;
        $model = new TransaksiModel();  
        $data_transaksi = [
            "no_anggota" => $this->request->getPost("no_anggota"),
            "tanggal_transaksi" => $tanggal_transaksi,
            "kode_transaksi" => $kode_transaksi,
            "jenis_transaksi" => $this->request->getPost("jenis_transaksi"),
            "jumlah_transaksi" => $this->request->getPost("jumlah_transaksi"),
            "created_at" => date("Y-m-d H:i:s"),
        ];

        if($model->save($data_transaksi)){
            $transaksi_id = $model->getInsertID();
            if(in_array($kode_transaksi,array("01","02","03","05","07","08"))){
                $model_transaksi = new TransaksiPemasukanModel();  
                $data_save = [
                    "transaksi_id" => $transaksi_id,
                    "simpanan_pokok" => ($kode_transaksi == "01" ? (int)$this->request->getPost("jumlah_transaksi") : 0),
                    "simpanan_wajib" => ($kode_transaksi == "02" ? (int)$this->request->getPost("jumlah_transaksi") : 0),
                    "simpanan_sukarela" => ($kode_transaksi == "03" ? (int)$this->request->getPost("jumlah_transaksi") : 0),
                    "cicilan_pinjaman" => ($kode_transaksi == "05" ? (int)$this->request->getPost("jumlah_transaksi") : 0),
                    "tambah_jasa" => ($kode_transaksi == "07" ? (int)$this->request->getPost("jumlah_transaksi") : 0),
                    "pengambilan_sukarela" => ($kode_transaksi == "08" ? (int)$this->request->getPost("jumlah_transaksi") : 0)
                ];    
            }else{
                $model_transaksi = new TransaksiPengeluaranModel();
                $data_save = [
                    "transaksi_id" => $transaksi_id,
                    "pinjaman" => ($kode_transaksi == "04" ? (int)$this->request->getPost("jumlah_transaksi") : 0),
                    "jasa" => ($kode_transaksi == "06" ? (int)$this->request->getPost("jumlah_transaksi") : 0)
                ];    
            }
            
            if($model_transaksi->save($data_save)){
                return json_encode(array("success"=>true,"info"=>"Data berhasil disimpan" ,"data"=>null));
            }else{
                return json_encode(array("success"=>false,"info"=>"Data gagal disimpan" ,"data"=>null));;;
            }
        }

        return false;

             
        
    }
}
