<?php

namespace App\Controllers;
use App\Models\PinjamanModel;
use Config\Services;
class Master_pinjaman extends BaseController
{
	public function index()
	{
        $model = new PinjamanModel();
        $data = [
            'data_list' => $model->paginate(10,'bootstrap'),
            'pager' => $model->pager,
        ];
        if($this->request->getPost("simpan")){
            return $this->save_pinjaman(); 
        }
        return view('master/pinjaman/index',$data);
	}

    public function list_pinjaman(){
        $model = new PinjamanModel();
        $data = [
            'data' => $model->paginate(10),
            'pager' => $model->pager,
        ];
        return $data;
    }

    public function delete($id){
        $session = session();
        $model = new PinjamanModel();
        if($model->where('pinjaman_id', $id)->delete()){
            $session->setFlashdata("message_success","Data Berhasil dihapus");
            return redirect()->to('/master_pinjaman');
        }else{
            $session->setFlashdata("message_failed","Data gagal Dihapus");
            return redirect()->to('/master_pinjaman');
        }
    }


    public function detail(){
        $session = session();
        $model = new PinjamanModel();
        $id = $this->request->getGet("pinjaman_id");
        $data = $model->where('pinjaman_id', $id)->first();
       
        return json_encode($data);
    }

    protected function save_pinjaman(){
        $session = session();
        $model = new PinjamanModel();
        $data = [
            'nama_pinjaman'     => $this->request->getPost('nama_pinjaman'),
            'created_at'    => date("Y-m-d H:i:s")
        ];
        if($model->save($data)){
            $session->setFlashdata("message_success","Data Berhasil Disimpan");
            return redirect()->to('/master_pinjaman');
        }else{
            $session->setFlashdata("message_failed","Data gagal Disimpan");
            return redirect()->to('/master_pinjaman');
        }
    } 

    public function update(){
        $session = session();
        $model = new PinjamanModel();
        $data = [
            'nama_pinjaman'     => $this->request->getPost('nama_pinjaman'),
            'updated_at'    => date("Y-m-d H:i:s")
        ];
        $id = $this->request->getPost("pinjaman_id");
        $update = $model->where("pinjaman_id",$id)->set($data)->update();
        if($update){
            $session->setFlashdata("message_success","Data Berhasil Diubah");
            return redirect()->to('/master_pinjaman');
        }else{
            $session->setFlashdata("message_failed","Data gagal Diubah");
            return redirect()->to('/master_pinjaman');
        }
    }
}
