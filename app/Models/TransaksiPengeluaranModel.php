<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class TransaksiPengeluaranModel extends Model{
    protected $table = 'transaksi_pengeluaran';
    protected $allowedFields = ['transaksi_pengeluaran_id','transaksi_id','no_anggota','status_transaksi','pinjaman','lama_pinjaman','jasa'];
}