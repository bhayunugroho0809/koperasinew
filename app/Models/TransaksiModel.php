<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class TransaksiModel extends Model{
    protected $table = 'transaksi';
    protected $allowedFields = ['transaksi_id','no_anggota','kode_transaksi','tanggal_transaksi','jenis_transaksi','jumlah_transaksi','created_at','updated_at','created_by','updated_by'];
}