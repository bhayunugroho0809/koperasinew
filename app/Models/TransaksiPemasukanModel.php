<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class TransaksiPemasukanModel extends Model{
    protected $table = 'transaksi_pemasukan';
    protected $allowedFields = ['transaksi_pemasukan_id','transaksi_id','no_anggota','simpanan_pokok','simpanan_wajib','simpanan_sukarela','cicilan_pinjaman','penarikan_sukarela','jasa_dibayar'];
}