<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class PinjamanModel extends Model{
    protected $table = 'master_pinjaman';
    protected $allowedFields = ['pinjaman_id','nama_pinjaman','jumlah','created_at','updated_at'];
}