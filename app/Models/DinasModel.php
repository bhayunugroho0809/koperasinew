<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class DinasModel extends Model{
    protected $table = 'master_dinas';
    protected $allowedFields = ['dinas_id','nama_dinas','created_at','updated_at'];
}