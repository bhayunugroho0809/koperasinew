<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class KelompokModel extends Model{
    protected $table = 'master_kelompok';
    protected $allowedFields = ['kelompok_id','nama_kelompok','created_at','updated_at'];
}