<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class KecamatanModel extends Model{
    protected $table = 'master_kecamatan';
    protected $allowedFields = ['kecamatan_id','nama_kecamatan','created_at','updated_at'];
}