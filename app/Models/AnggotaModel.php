<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class AnggotaModel extends Model{
    protected $table = 'master_anggota';
    protected $allowedFields = ['anggota_id','nik_anggota','nama_anggota','no_anggota','dinas','tanggal_masuk','alamat','telepon1','telepon2','pekerjaan','pendidikan','agama','rt','rw','kode_pos','kecamatan','kelompok','tanggal_lahir','tempat_lahir','status','jenis_kelamin','data_penjamin','nama_penjamin','nik_penjamin','agama_penjamin','telepon_penjamin','alamat_penjamin','alamat_kantor_penjamin','pendidikan_penjamin','pekerjaan_penjamin','created_at','updated_at','created_by','updated_by'];
}