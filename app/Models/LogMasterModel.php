<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class LogMasterModel extends Model{
    protected $table = 'log_master';
    protected $allowedFields = ['log_id','bulan','tahun','keterangan','previlege_insert','previlege_update','previlege_delete','created_at','updated_at','created_by','updated_by'];
}