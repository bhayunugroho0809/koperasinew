<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class SimpananModel extends Model{
    protected $table = 'master_simpanan';
    protected $allowedFields = ['simpanan_id','nama_simpanan','jumlah','created_at','updated_at'];
}