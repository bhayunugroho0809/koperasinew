<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>
<div class="col-md-12 mt-2 mb-2">
    <div class="card mb-2">
        <div class="card-header">
            <div class="card-title">Perhitungan SHU</div>
        </div> 
        <div class="card-body">
            <form method="GET">
            <table>
				<tr>
					<td>
						Tanggal Cetak
					</td>
					<td>&nbsp;</td>
					<td>
						<input id="tgl_cetak" name="tgl_cetak" class="dtpicker"value="<?php echo (!empty($_GET['tgl_cetak']) ? $_GET['tgl_cetak'] : "")  ?>" style="width:200px; height:25px" class="">
					</td>
				</tr>
				<tr>
					<td>
						Tahun
					</td>
					<td>&nbsp;</td>
					<td>
						<div id="filter_tgl" class="input-group" style="display: inline;">
                            <input id="tahun" name="tahun" value="<?php echo (!empty($_GET['tahun']) ? $_GET['tahun'] : "")  ?>" style="width:200px; height:25px" class="">
		
						</div>
					</td>
				</tr>
				<tr>
					<td>
						Total SHU
					</td>
					<td>&nbsp;</td>
					<td>
						<input id="total_shu" name="total_shu" value="<?php echo (!empty($_GET['total_shu']) ? $_GET['total_shu'] : "")  ?>" style="width:200px; height:25px" class="">
					</td>					
				</tr>
				</table>
				
				<hr/>
		
                <table style="float:left;width:40%">
                    <tr>
                        <td>SHU</td>
                    </tr>
                    <tr>
                        
                        <td>
                            Simpanan
                        </td>
                        
                        <td>&nbsp;</td>
                        
                        <td>
                            <input id="persen_simpanan" name="persen_simpanan" value="20" style="width:40px; height:25px" class="">
                        </td>
                        <td style="width:30px;">% =
                        </td>
                        <?php 
                            $total_shu_simpanan = 0;
                            $total_shu_simpanan_tampil =  0;

                            if(!empty($_GET)){
                                $total_shu_simpanan =  ($_GET['total_shu'] * $_GET['persen_simpanan'] / 100);
                                $total_shu_simpanan_tampil =  ($_GET['total_shu'] * $_GET['persen_simpanan'] / 100);
                            }
                        
                        ?>
                        <td>
                            <input id="hasil_simpanan_deviden" readonly name="total_simpanan" value="<?php echo number_format($total_shu_simpanan_tampil); ?>" style="background-color:#eee; width:200px; height:25px" class="">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Pinjaman
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <input id="persen_pinjaman" name="persen_pinjaman" value="25" style="width:40px; height:25px" class="">
                        </td>
                        <td style="width:30px;">% =
                        </td>
                        <?php 
                            $total_shu_pinjaman =  0;
                            $total_shu_pinjaman_tampil =  0;
                            if(!empty($_GET)){
                                $total_shu_pinjaman =  ($_GET['total_shu'] * $_GET['persen_pinjaman'] / 100);
                                $total_shu_pinjaman_tampil =  ($_GET['total_shu'] * $_GET['persen_pinjaman'] / 100);    
                            }
                        ?>
                        <td>
                            <input id="hasil_pinjaman_deviden" readonly name="total_pinjaman" value="<?php echo number_format($total_shu_pinjaman_tampil) ?>" style="background-color:#eee; width:200px; height:25px" class="">
                        </td>
                    </tr>
                </table>
                
                <center>
                    <button type="submit" class="btn btn-primary btn-sm"><i class="fas fa-search"></i> Cari</button>
                    <button class="btn btn-info btn-sm"><i class="fas fa-print"></i>  Cetak</button>
                </center>
            </form>
        </div>
    </div>
    <?php if(!empty($_GET["jenis_laporan"])): ?>
        <?php if($_GET["jenis_laporan"]=="1"): ?>
            <?= $this->include('laporan/v_pemasukan_harian') ?>  
        <?php elseif($_GET["jenis_laporan"]=="2"): ?>
            <?= $this->include('laporan/v_pengeluaran_harian') ?>  
        <?php endif; ?>
   <?php endif; ?>
    
</div>
<?= $this->endSection() ?>