<div class="card">
        <div class="card-header">
            <div class="card-title">Laporan Pemasukan Harian</div>
        </div>
        <div class="card-body">
            <table  class="table table-bordered body-table">
                    <?php
                        $total_simpanan_v = 0;
                        $total_simpanan_h = 0;
                        $total_simpanan_pokok = 0;
                        $total_simpanan_wajib = 0;
                        $total_simpanan_sukarela = 0;
                        $total_cicilan_pinjaman = 0;
                        $total_jasa_dibayar = 0;
                    ?>
                    <?php foreach($list_data AS $k=>$data_pasien): ?>
                        <tr>
                            <td style="width:5%; border:none;vertical-align: middle; font-weight:bold;text-align:left" colspan="8" > Kelompok :<?= ucwords(strtolower($k)) ?> </td>
                        </tr>
                        <tr class="header_kolom">
                            <th style="width:5%; vertical-align: middle; text-align:center" rowspan="2" > No. </th>
                            <th style="width:35%; vertical-align: middle; text-align:center" rowspan="2">No Anggota </th>
                            <th style="width:35%; vertical-align: middle; text-align:center" rowspan="2">Nama Anggota </th>
                            <th style="width:20%; vertical-align: middle; text-align:center" colspan="3"> Simpanan  </th>
                            <th style="width:20%; vertical-align: middle; text-align:center" rowspan="2"> Pinjaman  </th>
                        </tr>
                        <tr class="header_kolom">
                            <th style="vertical-align: middle; text-align:center" > Pokok</th>
                            <th style="vertical-align: middle; text-align:center">Wajib </th>
                            <th style="vertical-align: middle; text-align:center"> Sukarela  </th>
                        </tr>
                        <?php foreach($data_pasien AS $key=>$v): ?>
                            <?php
                                $total_simpanan_h = $v["simpanan_pokok"] + $v["simpanan_sukarela"] + $v["simpanan_wajib"];
                                $total_simpanan_v += $total_simpanan_h;
                                $total_simpanan_pokok += $v["simpanan_pokok"];
                                $total_simpanan_wajib += $v["simpanan_wajib"];
                                $total_simpanan_sukarela += $v["simpanan_sukarela"];
                                $total_cicilan_pinjaman += $v["pinjaman"];
                            ?>
                            <tr>
                                <td><?= ($key+1) ?></td>
                                <td><?= $v["no_anggota"] ?></td>
                                <td><?= $v["nama_anggota"] ?></td>
                                <td style="text-align:right"><?= number_format($v["simpanan_pokok"]); ?></td>
                                <td style="text-align:right"><?= number_format($v["simpanan_wajib"]); ?></td>
                                <td style="text-align:right"><?= number_format($v["simpanan_sukarela"]); ?></td>
                                <td style="text-align:right"><?= number_format($v["pinjaman"]); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                <tfooter>
                    <tr>
                        <td colspan="3" style="text-align:center">Total</td>
                        <td style="text-align:right;font-weight:bold"><?= number_format($total_simpanan_pokok); ?></td>
                        <td style="text-align:right;font-weight:bold"><?= number_format($total_simpanan_wajib); ?></td>
                        <td style="text-align:right;font-weight:bold"><?= number_format($total_simpanan_sukarela); ?></td>
                        <td style="text-align:right;font-weight:bold"><?= number_format($total_cicilan_pinjaman); ?></td>
                    </tr>
                </tfooter>
            </table>
        </div>
    </div>