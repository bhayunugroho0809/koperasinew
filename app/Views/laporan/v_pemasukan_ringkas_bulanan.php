<div class="card">
        <div class="card-header">
            <div class="card-title">Laporan Pemasukan Ringkas Bulanan</div>
        </div>
        <div class="card-body">
            <table  class="table table-bordered body-table">
                <thead class="thead">
                    <tr>
                        <th rowspan="2">Tanggal</th>
                        <th colspan="4">Simpanan</th>
                        <th colspan="2">Pinjaman</th>
                        <th rowspan="2">Jasa Dibayar</th>
                    </tr>
                    <tr>
                        <th>Pokok</th>
                        <th>Wajib</th>				
                        <th>Sukarela</th>
                        <th>Total</th>
                        <th>Cicilan Pinjaman</th>
                        <th>Sisa</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $total_simpanan_v = 0;
                        $total_simpanan_h = 0;
                        $total_simpanan_pokok = 0;
                        $total_simpanan_wajib = 0;
                        $total_simpanan_sukarela = 0;
                        $total_cicilan_pinjaman = 0;
                        $total_jasa_dibayar = 0;
                    ?>
                    <?php foreach($list_data AS $k=>$v): ?>
                    <?php
                        $total_simpanan_h = $v["simpanan_pokok"] + $v["simpanan_sukarela"] + $v["simpanan_wajib"];
                        $total_simpanan_v += $total_simpanan_h;
                        $total_simpanan_pokok += $v["simpanan_pokok"];
                        $total_simpanan_wajib += $v["simpanan_wajib"];
                        $total_simpanan_sukarela += $v["simpanan_sukarela"];
                        $total_cicilan_pinjaman += $v["cicilan_pinjaman"];
                        $total_jasa_dibayar += $v["jasa_dibayar"];
                    ?>
                    <tr>
                        <td><?= $v["hari"] ?></td>
                        <td style="text-align:right"><?= number_format($v["simpanan_pokok"]); ?></td>
                        <td style="text-align:right"><?= number_format($v["simpanan_wajib"]); ?></td>
                        <td style="text-align:right"><?= number_format($v["simpanan_sukarela"]); ?></td>
                        <td style="text-align:right"><?= number_format($total_simpanan_h); ?></td>
                        <td style="text-align:right"><?= number_format($v["cicilan_pinjaman"]); ?></td>
                        <td style="text-align:right"><?= number_format($v["cicilan_pinjaman"]); ?></td>
                        <td style="text-align:right"><?= number_format($v["jasa_dibayar"]); ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfooter>
                    <tr>
                        <td>Total</td>
                        <td style="text-align:right;font-weight:bold"><?= number_format($total_simpanan_pokok); ?></td>
                        <td style="text-align:right;font-weight:bold"><?= number_format($total_simpanan_wajib); ?></td>
                        <td style="text-align:right;font-weight:bold"><?= number_format($total_simpanan_sukarela); ?></td>
                        <td style="text-align:right;font-weight:bold"><?= number_format($total_simpanan_v); ?></td>
                        <td style="text-align:right;font-weight:bold"><?= number_format($total_cicilan_pinjaman); ?></td>
                        <td style="text-align:right;font-weight:bold"><?= number_format($total_simpanan_pokok); ?></td>
                        <td style="text-align:right;font-weight:bold"><?= number_format($total_jasa_dibayar); ?></td>
                    </tr>
                </tfooter>
            </table>
        </div>
    </div>