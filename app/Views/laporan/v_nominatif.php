
    <div class="card">
        <div class="card-header">
            <div class="card-title">Laporan Pemasukan Harian</div>
        </div>
        <div class="card-body">
            <table  class="table table-bordered body-table">
                    <?php
                        $total_simpanan_v = 0;
                        $total_simpanan_h = 0;
                        $total_simpanan_pokok = 0;
                        $total_simpanan_wajib = 0;
                        $total_simpanan_sukarela = 0;
                        $total_cicilan_pinjaman = 0;
                        $total_jasa_dibayar = 0;
                        $total_pokok = 0;
                        $total_wajib = 0;
                        $total_sukarela = 0;
                        $total_jml_simpanan = 0;
                        $total_pinjaman_pokok = 0;
                        $total_dibayar = 0;
                     
                        $jumlah_subtotal = 0;
                    ?>
                    <?php foreach($list_data AS $k=>$data_pasien): ?>
                        <tr>
                            <td style="width:5%; border:none;vertical-align: middle; font-weight:bold;text-align:left" colspan="8" > Area :<?= ucwords(strtolower($k)) ?> </td>
                        </tr>
                        <tr class="header_kolom">
                            <th style="width:5%; vertical-align: middle; text-align:center" rowspan="2" > No. </th>
                            <th style="width:35%; vertical-align: middle; text-align:center" rowspan="2">No Anggota </th>
                            <th style="width:35%; vertical-align: middle; text-align:center" rowspan="2">Nama Anggota </th>
                            <th style="width:20%; vertical-align: middle; text-align:center" colspan="4"> Simpanan  </th>
                            <th style="width:20%; vertical-align: middle; text-align:center" colspan="2"> Pinjaman  </th>
                            <th style="width:20%; vertical-align: middle; text-align:center" rowspan="2"> Jasa dibayar </th>
                        </tr>
                        <tr class="header_kolom">
                            <th style="vertical-align: middle; text-align:center" >Pokok</th>
                            <th style="vertical-align: middle; text-align:center">Wajib </th>
                            <th style="vertical-align: middle; text-align:center"> Sukarela  </th>
                            <th style="vertical-align: middle; text-align:center"> Jumlah  </th>
                            <th style="vertical-align: middle; text-align:center"> Pokok  </th>
                            <th style="vertical-align: middle; text-align:center"> dibayar  </th>
                        </tr>
                        <?php
                            $subtotal_pokok = 0;
                            $subtotal_wajib = 0;
                            $subtotal_sukarela = 0;
                            $subtotal_cicilan = 0;
                            $subtotal_pinjaman_pokok  = 0;
                            $subtotal_jasa = 0;
                            $subtotal_jml_simpanan= 0;
                            $subtotal_jasa_dibayar = 0;
                            $subtotal_dibayar = 0;
                            $subtotal_sisa_jasa = 0;

                            $jumlah_subtotal = 0;

                            $total_jasa_dibayar = $subtotal_jasa_dibayar;
                            $total_sisa_jasa = $subtotal_sisa_jasa;
                        
                        ?>
                        <?php foreach($data_pasien AS $key=>$v): ?>
                            <?php $jml  =$jumlah_subtotal + $v['sisa']; ?>
                            <?php
                                $total_simpanan_h = $v["simpanan_pokok"] + $v["simpanan_sukarela"] + $v["simpanan_wajib"];
                                $total_simpanan_v += $total_simpanan_h;
                                $total_simpanan_pokok += $v["simpanan_pokok"];
                                $total_simpanan_wajib += $v["simpanan_wajib"];
                                $total_simpanan_sukarela += $v["simpanan_sukarela"];
                                $total_jasa_dibayar += $v["jasa_dibayar"];
                            ?>
                            <tr>
                                <td><?= ($key+1) ?></td>
                                <td><?= $v["no_anggota"] ?></td>
                                <td><?= $v["nama_anggota"] ?></td>
                                <td style="text-align:right"><?= number_format($v["simpanan_pokok"]); ?></td>
                                <td style="text-align:right"><?= number_format($v["simpanan_wajib"]); ?></td>
                                <td style="text-align:right"><?= number_format($v["simpanan_sukarela"]); ?></td>
                                <td style="text-align:right"><?= number_format($v["jumlah_simpanan"]); ?></td>
                                <td style="text-align:right"><?= number_format($jml); ?></td>
                                <td style="text-align:right"><?= number_format($v["jasa_dibayar"]); ?></td>
                            </tr>
                            <?php
                                $subtotal_pokok += $v['simpanan_pokok'];
                                $subtotal_wajib += $v['simpanan_wajib'];
                                $subtotal_sukarela += $v['simpanan_sukarela'];
                                $subtotal_pinjaman_pokok += $v['pokok'];			
                                $subtotal_dibayar += $v['jumlah_bayar'];
                                $subtotal_jml_simpanan += $v['jumlah_simpanan'];			
                                $subtotal_jasa_dibayar += $v['jasa_dibayar'];
                                $subtotal_sisa = $jml;
                            ?>
                        <?php endforeach; ?>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Sub Total</th>
                            <th><?= number_format($subtotal_pokok); ?></th>
                            <th><?= number_format($subtotal_wajib); ?></th>
                            <th><?= number_format($subtotal_sukarela); ?></th>
                            <th><?= number_format($subtotal_jml_simpanan); ?></th>
                            <th><?= number_format($subtotal_pinjaman_pokok); ?></th>
                            <th><?= number_format($subtotal_sisa); ?></th>
                            <th><?= number_format($subtotal_jasa_dibayar); ?></th>
                        </tr>
                        <?php
                            $total_pokok += $subtotal_pokok;
                            $total_wajib += $subtotal_wajib;
                            $total_sukarela += $subtotal_sukarela;
                            $total_jml_simpanan += $subtotal_jml_simpanan;
                            $total_pinjaman_pokok = $subtotal_pinjaman_pokok;
                            $total_dibayar = $subtotal_dibayar;
                            $jumlah_subtotal = $jumlah_subtotal +  $v['sisa'];

                            $total_jasa_dibayar += $subtotal_jasa_dibayar;
                            $total_sisa_jasa += $subtotal_sisa_jasa;
                        ?>
                    <?php endforeach; ?>
                <tfooter>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Total</th>
                        <th><?= number_format($total_pokok); ?></th>
                        <th><?= number_format($total_wajib); ?></th>
                        <th><?= number_format($total_sukarela); ?></th>
                        <th><?= number_format($total_jml_simpanan); ?></th>
                        <th><?= number_format($total_pinjaman_pokok); ?></th>
                        <th><?= number_format($jumlah_subtotal); ?></th>
                        <th><?= number_format($total_jasa_dibayar); ?></th>                     
                    </tr>
                </tfooter>
            </table>
        </div>
    </div>