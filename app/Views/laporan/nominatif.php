<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>
<div class="col-md-12 mt-2 mb-2">
    <div class="card mb-2">
        <div class="card-header">
            <div class="card-title">Pencarian - Laporan Transaksi Harian</div>
        </div> 
        <div class="card-body">
            <form method="GET">
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Tahun</label>
                    <div class="form-inline col-md-3">
                        <input type="text" class="form-control col-md-3" id="tahun" value="<?= (!empty($_GET['tahun']) ?  $_GET['tahun'] : '')?>"  name="tahun" placeholder="----">
                    </div> 
                </div>
                <center>
                    <button type="submit" class="btn btn-primary btn-sm"><i class="fas fa-search"></i> Cari</button>
                    <button class="btn btn-info btn-sm"><i class="fas fa-print"></i>  Cetak</button>
                </center>
            </form>
        </div>
    </div>
    <?php if(!empty($_GET)): ?>
        <?= $this->include('laporan/v_nominatif') ?>  
    <?php endif; ?>
</div>
<?= $this->endSection() ?>