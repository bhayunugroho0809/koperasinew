<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>
<div class="col-md-12 mt-2 mb-2">
    <div class="card mb-2">
        <div class="card-header">
            <div class="card-title">Pencarian</div>
        </div> 
        <div class="card-body">
            <form method="GET">
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Bulan/Tahun</label>
                    <div class="form-inline col-md-3">
                        <input type="text" class="form-control col-md-2" id="bulan" value="<?= (!empty($_GET['bulan']) ?  $_GET['bulan'] : '')?>"name="bulan" placeholder="--"> / 
                        <input type="text" class="form-control col-md-3" id="tahun" value="<?= (!empty($_GET['tahun']) ?  $_GET['tahun'] : '')?>"  name="tahun" placeholder="----">
                    </div> 
                </div>
                <div class="row">
                    <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Laporan</label>
                    <div class="col-sm-8 form-inline">
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="jenis_laporan" id="jenis_laporanl" value="1">
							<label class="form-check-label" for="jenis_laporanl">Pemasukan</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="jenis_laporan" id="jenis_laporan2" value="2">
							<label class="form-check-label" for="jenis_laporan2">Pengeluaran</label>
						</div>
                    </div>
                 </div>
                <center>
                    <button type="submit" class="btn btn-primary btn-sm"><i class="fas fa-search"></i> Cari</button>
                    <button class="btn btn-info btn-sm"><i class="fas fa-print"></i>  Cetak</button>
                </center>
            </form>
        </div>
    </div>
    <?php if(!empty($_GET["jenis_laporan"])): ?>
        <?php if($_GET["jenis_laporan"]=="1"): ?>
            <?= $this->include('laporan/v_pemasukan_bulanan') ?>  
        <?php elseif($_GET["jenis_laporan"]=="2"): ?>
            <?= $this->include('laporan/v_pengeluaran_bulanan') ?>  
        <?php endif; ?>
   <?php endif; ?>
    
</div>
<?= $this->endSection() ?>