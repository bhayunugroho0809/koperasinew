<div class="card">
        <div class="card-header">
            <div class="card-title">Laporan Pengeluaran Ringkas Bulanan</div>
        </div>
        <div class="card-body">
            <table  class="table table-bordered body-table">
                <thead class="thead">
                    <tr>
                        <th style="text-align:center" rowspan="2">Tanggal</th>
                        <th colspan="4" style="text-align:center">pengambilan</th>
                        <th style="text-align:center" rowspan="2">Pinjaman</th>
                        <th style="text-align:center" rowspan="2">Jasa</th>
                    </tr>
                    <tr>
                        <th style="text-align:center">Pokok</th>
                        <th style="text-align:center">Wajib</th>				
                        <th style="text-align:center">Sukarela</th>
                        <th style="text-align:center">Total</th>
                    </tr>
                </thead>
                <tbody>          
                    <?php
                        $total_pengambilan_v = 0;
                        $total_pengambilan_h = 0;
                        $total_pengambilan_pokok = 0;
                        $total_pengambilan_wajib = 0;
                        $total_pengambilan_sukarela = 0;
                        $total_pinjaman = 0;
                        $total_jasa = 0;
                    ?>
                    <?php foreach($list_data AS $k=>$v): ?>
                    <?php
                        $total_pengambilan_h = $v["pengambilan_pokok"] + $v["pengambilan_sukarela"] + $v["pengambilan_wajib"];
                        $total_pengambilan_v += $total_pengambilan_h;
                        $total_pengambilan_pokok += $v["pengambilan_pokok"];
                        $total_pengambilan_wajib += $v["pengambilan_wajib"];
                        $total_pengambilan_sukarela += $v["pengambilan_sukarela"];
                        $total_pinjaman += $v["pinjaman"];
                        $total_jasa += $v["jasa"];
                    ?>
                    <tr>
                        <td><?= $v["hari"] ?></td>
                        <td style="text-align:right"><?= number_format($v["pengambilan_pokok"]); ?></td>
                        <td style="text-align:right"><?= number_format($v["pengambilan_wajib"]); ?></td>
                        <td style="text-align:right"><?= number_format($v["pengambilan_sukarela"]); ?></td>
                        <td style="text-align:right"><?= number_format($total_pengambilan_h); ?></td>
                        <td style="text-align:right"><?= number_format($v["pinjaman"]); ?></td>
                        <td style="text-align:right"><?= number_format($v["jasa"]); ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfooter>
                        <tr>
                            <td>Total</td>
                            <td style="text-align:right;font-weight:bold"><?= number_format($total_pengambilan_pokok); ?></td>
                            <td style="text-align:right;font-weight:bold"><?= number_format($total_pengambilan_wajib); ?></td>
                            <td style="text-align:right;font-weight:bold"><?= number_format($total_pengambilan_sukarela); ?></td>
                            <td style="text-align:right;font-weight:bold"><?= number_format($total_pengambilan_v); ?></td>
                            <td style="text-align:right;font-weight:bold"><?= number_format($total_pinjaman); ?></td>
                            <td style="text-align:right;font-weight:bold"><?= number_format($total_jasa); ?></td>
                        </tr>
                </tfooter>
            </table>
        </div>
    </div>