<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>
<div class="col-md-12 mt-2 mb-2">
    <div class="row">
        <div class="col-md-6">
                <div class="card">
                    <div class="card-body card-max-height">
                            <div class="row">                                
                                <label for="" class="col-md-3 col-form-label text-right required">No Anggota</label>
                                <form method="get" class="col-md-6">
                                    <div class="input-group mb-3">
                                    <select class="custom-select" id="no_anggota" onChange="return showBiodata(this.value)" name="no_anggota">
                                        <option value="">-Pilih Anggota-</option>
                                        <?php foreach($list_anggota AS $anggota): ?>
                                            <option value="<?php echo $anggota['no_anggota'] ?>"><?php echo $anggota['no_anggota']." - ".$anggota['nama_anggota'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <div class="input-group-append">
                                        <label class="bg-primary input-group-text" for="inputGroupSelect02"><i class="fas fa-search text-white"></i></label>
                                    </div>
                                    </div>
                                </form>
                            </div>
                            <form method="post"> 
                            <div class="row">
                                <label for="" class="col-sm-3 text-right col-form-label required">Tanggal Transaksi</label>
                                <input type="hidden"  name="no_anggota_post" value="<?= (!empty($_GET["no_anggota"]) ? $_GET["no_anggota"] : "") ; ?>" class="form-control" placeholder="No Anggota ">                                  
                                <div class="col-auto">
                                    <div class=" input-group mb-3">
                                        <input type="text" id="tanggal_transaksi" name="tanggal_transaksi" required value="" class="form-control datepicker" placeholder="dd/mm/yyyy">
                                        <div class="input-group-append ">
                                            <button name="cari" type="submit" class="bg-primary datepicker"><span class="" id="basic-addon2"><i class="fas fa-calendar text-white"></i> </span></button>
                                        </div>
                                    </div> 
                                </div> 
                                <label for="" class="col-auto text-right col-form-label">No Transaksi</label>
                                <input type="text"  name="no_transaksi" value="<?= $count_transaksi; ?>" class="form-control col-md-3" readonly placeholder="No Transaksi"> 
                            </div>
                            <div class="row">
                                <label for="" class="col-sm-3 text-right col-form-label required">Kode Transaksi</label>
                                <div class="form-inline col-md-9">
                                    <input type="text"  name="kode_transaksi" id="kode_transaksi" required onChange="return showTrans(this,event)" value="" class="form-control col-md-2" placeholder="">
                                    <input type="text"  readonly id="nama_transaksi"required value="" class="form-control-plaintext col-md-9" placeholder="">
                                </div> 
                            </div>
                            <div class="row">
                                <label for="" class="col-sm-3 text-right col-form-label jenis_label">Jenis</label>
                                <div class="col-md-9 form-inline" >
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio"  name="tipe_transaksi" id="tipe_transaksi0" value="D">
                                        <label class="form-check-label" for="tipe_transaksi1">Debit</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio"  name="tipe_transaksi" id="tipe_transaksi1" value="K">
                                        <label class="form-check-label" for="tipe_transaksi2">Kredit</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <label for="" class="col-sm-3 text-right col-form-label required" >Jumlah</label>
                                <div class="col-auto">
                                    <div class=" input-group mb-3">
                                        <input type="text"  name="jumlah_transaksi" id="jumlah_transaksi" required value="" class="form-control" placeholder="">
                                        <div class="input-group-append ">
                                        <span class="input-group-text" style="font-size:12px"id="basic-addon2">Rp.</span>
                                        </div>
                                    </div> 
                                </div> 
                                <button type="submit" name="simpan"  value="simpan" onClick="return submitForm(this,event)" class="btn btn-primary btn-xs" style="height:32px;vertical-align:baseline"><i class="fas fa-save" id="standby_mode"></i> <i  style="display:none" class="fas fa-spinner fa-pulse" id="process_mode"></i>Simpan </button>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body card-max-height">
                    <div class="row">
                        <label for="" class="col-sm-3 text-right col-form-label">Nama Anggota</label>
                        <div class="form-inline col-md-9">
                            <input type="text" id="nama_anggota" value="<?= (!empty($get_bio) ? $get_bio['nama_anggota'] : "") ?>" disabled class="form-control-plaintext col-md-9" placeholder="">
                            <input type="text" id="jenis_kelamin" value="<?= (!empty($get_bio) ? $get_bio['jenis_kelamin'] : "") ?>" class="form-control-plaintext col-md-2" placeholder="">
                        </div> 
                    </div>
                    <div class="row">
                        <label for="" class="col-sm-3 text-right col-form-label">Tempat /Tanggal Lahir</label>
                        <div class="form-inline col-md-9">
                            <input type="text" id="tempat_lahir" value="<?= (!empty($get_bio) ? $get_bio['tempat_lahir'] : "") ?>" class="form-control col-md-5" placeholder="">&nbsp;/&nbsp; 
                            <input type="text" id="tanggal_lahir" value="<?= (!empty($get_bio["tanggal_lahir"]) ? date_indo($get_bio['tanggal_lahir']) : "") ?>" class="form-control-plaintext col-md-3" placeholder="dd/mm/yyyy">
                        </div> 
                    </div>
                    <div class="row">
                        <label for="" class="col-sm-3 text-right col-form-label">Telepon</label>
                        <div class="form-inline col-md-9">
                            <input type="text" id="telepon1" value="<?= (!empty($get_bio) ? $get_bio['telepon'] : "") ?>" class="form-control col-md-5" placeholder="">&nbsp;&nbsp;&nbsp;
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" id="status" <?= (!empty($get_bio) ? "checked" : "") ?>  disabled type="checkbox" id="inlineCheckbox1" value="option1">
                                <label class="form-check-label" for="inlineCheckbox1">Aktif</label>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <label for="" class="col-sm-3 text-right col-form-label">Alamat</label>
                        <div class="col-md-9">
                            <input type="text" id="alamat" name="alamat" value="<?= (!empty($get_bio) ? $get_bio['alamat'] : "") ?>" class="form-control" placeholder="">
                        </div> 
                    </div>
                    <div class="row">
                        <label for="" class="col-sm-3 text-right col-form-label">Kelompok</label>
                        <div class="col-md-9">
                            <input type="text"  id="kelompok" name="kelompok" value="<?= (!empty($get_bio) ? $get_bio['nama_kelompok'] : "") ?>" class="form-control" placeholder="">
                        </div> 
                    </div>
                    <div class="row">
                        <label for="" class="col-sm-3 text-right col-form-label">Nama Penjamin</label>
                        <div class="col-md-9">
                            <input type="text"  id="nama_penjamin" name="nama_penjamin" value="<?= (!empty($get_bio) ? $get_bio['nama_penjamin'] : "") ?>" class="form-control" placeholder="">
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 mt-2 mb-2">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card-title float-left">Transaksi Simpan</div>
                        </div>
                        <div class="col-md-6">
                            <div class="card-title float-right"><a href="#" onClick="return showPrint()" ><i class="fas fa-print text-warning"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-max-height-transaction" id="body-simpan">
                    <table class="table table-stripped table-bordered">
                        <thead>
                            <tr>
                                <th rowspan="2">Tanggal</th>
                                <th rowspan="2">Kode</th>
                                <th colspan="2" style="text-align:center">Simpanan</th>
                                <th rowspan="2">Saldo</th>
                                <th rowspan="2">NP</th>
                                <th rowspan="2">P</th>
                            </tr>
                            <tr>
                                <th>Pokok</th>
                                <th>Sukarela</th>
                            </tr>
                        </thead>
                        <tbody id="body-content">
                           
                        </tbody>
                    </table>
                </div>
                <div class="card-footer card-max-height-footer">
                    <div class="row">
                        <div class="form-inline">
                            <label for="" class="col-sm-2 col-form-label">Pokok</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control col-auto" id="total_simpanan_pokok" name="total_simpanan_pokok" placeholder="0">
                            </div>
                            <label for="" class="col-sm-2 col-form-label">Wajib</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control col-auto" id="total_simpanan_wajib" name="total_simpanan_wajib" placeholder="0">
                            </div>                            
                            
                            <label for="" class="col-sm-2 col-form-label">Sukarela</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control col-auto" id="total_simpanan_sukarela" name="total_simpanan_sukarela" placeholder="0">
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Transaksi Pinjam</div>
                </div>
                <div class="card-body card-max-height-transaction" >
                    <table class="table table-stripped table-bordered">
                        <thead>
                            <tr>
                                <th rowspan="2">Tanggal</th>
                                <th rowspan="2">Kode</th>
                                <th colspan="2">Pinjaman</th>
                                <th rowspan="2">NP</th>
                                <th rowspan="2">P</th>
                            </tr>
                            <tr>
                                <th>Debit</th>
                                <th>Kredit</th>
                            </tr>
                        </thead>
                        <tbody id="body-content-pinjaman">
                        
                        </tbody>
                    </table>
                </div>
                <div class="card-footer card-max-height-footer">
                    <table>
                        <tr>
                            <th class="text-right">15/09/2021</th>
                            <th class="text-center">Pokok</th>
                            <th class="text-center">Jasa</th>
                            <th class="text-center">Jumlah</th>
                            <th class="text-center">Angsuran</th>
                        </tr>
                        <tr>
                            <td class="text-right">Pinjaman</td>
                            <td class="text-right"><input type="text" class="form-control" /></td>
                            <td class="text-right"><input type="text" class="form-control" /></td>
                            <td class="text-right"><input type="text" class="form-control" /></td>
                            <td class="text-right"><input type="text" class="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="text-right">Pembayaran</td>
                            <td class="text-right"><input type="text" class="form-control" /></td>
                            <td class="text-right"><input type="text" class="form-control" /></td>
                            <td class="text-right"><input type="text" class="form-control" /></td>
                            <td class="text-right"><input type="text" class="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="text-right">Sisa Pinjaman</td>
                            <td class="text-right"><input type="text" class="form-control" /></td>
                            <td class="text-right"><input type="text" class="form-control" /></td>
                            <td class="text-right"><input type="text" class="form-control" /></td>
                            <td class="text-right"><input type="text" class="form-control" /></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" id="print" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<form method="get" action="<?= base_url("transaksi/print_simpanan") ?>">	
				<input type="hidden" id="dinas_id" name="dinas_id" />
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Print Filter</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
                    
						<div class="row">
                            <label for="" class="col-sm-2 col-form-label required">Tanggal Transaksi</label>
                            <div class="col-md-10">
                                <div class="form-inline">
                                    <input type="text" class="form-control col-md-1" id="tanggal_awal" required name="tanggal_awal" placeholder="--"> 
                                    <input type="text" class="form-control col-md-1" id="bulan_awal"   required name="bulan_awal" placeholder="--">
                                    <input type="text" class="form-control col-md-2" id="tahun_awal"   required name="tahun_awal" placeholder="----">
                                    <label class="label">&nbsp;&nbsp;S/D&nbsp;&nbsp; </label>
                                    <input type="text" class="form-control col-md-1" id="tanggal_awal" required name="tanggal_awal" placeholder="--">   
                                    <input type="text" class="form-control col-md-1" id="bulan_awal"   required name="bulan_awal" placeholder="--">
                                    <input type="text" class="form-control col-md-2" id="tahun_awal"   required name="tahun_awal" placeholder="----">
                             
                                </div> 
                               
                            </div> 
						</div>
				   	</div>
					<div class="modal-footer">
                        <button type="submit" name="print" value="print" class="btn btn-primary">Print</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
    $no_anggota = (!empty($_GET["no_anggota"]) ? $_GET["no_anggota"] : "");
?>
<script>
    function showPrint(){
        $("#print").modal("show");
    }
    function showBiodata(value){
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '<?= base_url("master_anggota/anggota_detail") ?>',
            data: {
                no_anggota: value	
            },
            beforeSend: function(){
            },
            complete: function(){
           
            },
            success: function(data){
                if(data.success){
                    console.log(data.success);
                    $("#nama_anggota").val(data.data.nama_anggota);
                    $("#tempat_lahir").val(data.data.tempat_lahir);
                    $("#tanggal_lahir").val(data.data.tanggal_lahir);
                    $("#telepon1").val(data.data.telepon1);
                    $("#alamat").val(data.data.alamat);
                    $("#kelompok").val(data.data.nama_kelompok);
                    $("#nama_penjamin").val(data.data.nama_penjamin);
                    $("#jenis_kelamin").val(data.data.jenis_kelamin);
                    $("#status").attr("checked",true);
                    list_simpanan(data.data.no_anggota);
                    list_pinjaman(data.data.no_anggota);
                }else{
                    Swal.fire(
                        'Wajib Diisi',
                        'Nomor Anggota tidak diketahui',
                        'error'
                    );
                }
            }
         }).fail(function(error) {
             console.log(error);
         });    
    }
    function showTrans(identifier,e){
        e.preventDefault();
        var data = $(identifier).val();
        if(data == "01"){
            $("#nama_transaksi").val("")
            $("#nama_transaksi").val("Simpanan Pokok");
        }else if(data == "02"){
            $("#nama_transaksi").val("")
            $("#nama_transaksi").val("Simpanan Wajib");
        }else if(data == "03"){
            $("#nama_transaksi").val("")
            $("#nama_transaksi").val("Simpanan Sukarela");
        }else if(data == "04"){
            $("#nama_transaksi").val("")
            $("#nama_transaksi").val("Pinjaman");
        }else if(data == "05"){
            $("#nama_transaksi").val("")
            $("#nama_transaksi").val("Cicilan Pinjaman");
        }else if(data == "06"){
            $("#nama_transaksi").val("")
            $("#nama_transaksi").val("Jasa");
        }else if(data == "07"){
            $("#nama_transaksi").val("")
            $("#nama_transaksi").val("Tambah Jasa");
        }

        if(data == "04" || data == "05" || data == "06" || data == "07"){
            $('.form-check-input').prop("disabled", false);
            $('.jenis_label').addClass("required");
            $('.form-check-input').prop("required", true);
            
        }else{
            $('.form-check-input').prop("disabled", false);
           
            $('.jenis_label').removeClass("required");
        }
    }
    function list_simpanan(no_anggota_get){
        if(!no_anggota_get){
            return;
        }

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '<?= base_url("transaksi/list_simpanan") ?>',
            data: {
                no_anggota: no_anggota_get	
            },
            beforeSend: function(){
            },
            complete: function(){
           
            },
            success: function(data){
                var html = "";
                var saldo = 0;
                var total_simpanan_pokok = 0;
                var total_simpanan_wajib = 0;
                var total_simpanan_sukarela = 0;
               
                if(data.success){
                    $.each(data.data,function(index,value){
                        saldo += parseInt(value.simpanan_pokok) + parseInt(value.simpanan_wajib) + parseInt(value.simpanan_sukarela);  
                        total_simpanan_wajib += parseInt(value.simpanan_wajib);  
                        total_simpanan_sukarela += parseInt(value.simpanan_sukarela);  
                        total_simpanan_pokok += parseInt(value.simpanan_pokok);  
                        html += "<tr>/";
                        html += "<td>" + date_format(value.tanggal_transaksi) + "</td>";
                        html += "<td>" + value.kode_transaksi + "</td>";
                        html += "<td>" + number_format(value.simpanan_pokok) + "</td>";
                        html += "<td>" + number_format(value.simpanan_sukarela) + "</td>";
                        html += "<td>" + number_format(saldo) + "</td>";
                        html += "<td>-</td>";
                        html += "<td>-</td>";
                        html += "</tr>";
                    });
                    console.log(html);
                    $("#body-content").html(html);
                    $("#total_simpanan_pokok").val(total_simpanan_pokok);
                    $("#total_simpanan_sukarela").val(total_simpanan_sukarela);
                    $("#total_simpanan_wajib").val(total_simpanan_wajib);
                }else{
                    Swal.fire(
                        'Wajib Diisi',
                        'Nomor Anggota wajib diisi',
                        'error'
                    );
                }
            }
         }).fail(function(error) {
             console.log(error);
         });
    
    
    }

    
    function list_pinjaman(no_anggota_get){
        if(!no_anggota_get){
            return;
        }

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '<?= base_url("transaksi/list_pinjaman") ?>',
            data: {
                no_anggota: no_anggota_get	
            },
            beforeSend: function(){
            },
            complete: function(){
           
            },
            success: function(data){
                var html = "";
                var saldo = 0;
                if(data.success){
                    $.each(data.data,function(index,value){
                        saldo += parseInt(value.debit) - parseInt(value.kredit);  
                        html += "<tr>/";
                        html += "<td>" + date_format(value.tanggal_transaksi) + "</td>";
                        html += "<td>" + value.kode_transaksi + "</td>";
                        html += "<td>" + number_format(value.debit) + "</td>";
                        html += "<td>" + number_format(value.kredit) + "</td>";
                        html += "<td>" + number_format(saldo) + "</td>";
                        html += "<td>-</td>";
                        html += "<td>-</td>";
                        html += "</tr>";
                    });
                    $("#body-content-pinjaman").html(html);
                }else{
                    Swal.fire(
                        'Wajib Diisi',
                        'Nomor Anggota wajib diisi',
                        'error'
                    );
                }
            }
         }).fail(function(error) {
             console.log(error);
         });
    }

 
    
    function submitForm(identifier,e){
       e.preventDefault();
       
       var no_anggota_get = $("#no_anggota").val();
       var tanggal_transaksi_get = $("#tanggal_transaksi").val();
       var kode_transaksi_get = $("#kode_transaksi").val();
       var jumlah_transaksi_get = $("#jumlah_transaksi").val();
       var tipe_transaksi = $('input[name=tipe_transaksi]:checked').val()
      
     
       if(no_anggota_get == ""){
            Swal.fire(
                'Wajib Diisi',
                'Nomor Anggota wajib diisi',
                'warning'
            );
            e.preventDefault();
       }else if(tanggal_transaksi_get == ""){
            Swal.fire(
                'Wajib Diisi',
                'Tanggal Transaksi wajib diisi',
                'warning'
            );
            e.preventDefault();
       }else if(tipe_transaksi == ""){
            Swal.fire(
                'Wajib Diisi',
                'Jenis Transaksi wajib diisi',
                'warning'
            );
            e.preventDefault();
       }else if(kode_transaksi_get == ""){
            Swal.fire(
                'Wajib Diisi',
                'Kode Transaksi wajib diisi',
                'warning'
            );
            e.preventDefault();
       }else if(jumlah_transaksi_get == ""){
            Swal.fire(
                'Wajib Diisi',
                'Jumlah Transaksi wajib diisi',
                'warning'
            );
            e.preventDefault();
       }else{
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '<?= base_url("transaksi/save_form") ?>',
                data: {
                    no_anggota: no_anggota_get,					
                    tanggal_transaksi: tanggal_transaksi_get,					
                    kode_transaksi: kode_transaksi_get,					
                    jumlah_transaksi: jumlah_transaksi_get,					
                    jenis_transaksi: tipe_transaksi,					
                },
                beforeSend: function(){
                    $("#standby_mode").hide();
                    $("#process_mode").show();
                },
                complete: function(){
                    $("#standby_mode").show();
                    $("#process_mode").hide();
                },success: function(data){
                    if(data.success){
                        Swal.fire(
                            'Sukses',
                            data.info,
                            'success'
                        );
                        list_simpanan(no_anggota_get);
                        list_pinjaman(no_anggota_get);
                    }else{ 
                        Swal.fire(
                            'Error',
                            data.info,
                            'error'
                        );

                    }
                }
            }).fail(function(error) {
                console.log(error);
            });	
       }
    }
    $(function(){
        selectopt_basic('#no_anggota');
    });
</script>
<?= $this->endSection() ?>