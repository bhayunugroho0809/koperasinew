<nav aria-label="Page navigation example">
	<ul class="pagination justify-content-end">

        <?php if ($pager->hasPrevious()) : ?>
            <li class="page-item disabled">
		        <a class="page-link" href="<?= $pager->getPrevious() ?>" tabindex="-1">Previous</a>
		    </li>
        <?php endif ?>
		
        <?php foreach ($pager->links() as $link) : ?>
            <!--<li <?= $link['active'] ? 'class="active page-item"' : 'class="page-item"' ?>>
                <a href="<?= $link['uri'] ?>" class="page-link">
                    <?= $link['title'] ?>
                </a>
            </li>-->
            <li class="page-item"><a class="page-link" href="<?= $link['uri'] ?>"><?= $link['title'] ?></a></li>
        <?php endforeach ?>
        <?php if ($pager->hasNext()) : ?>
            <li class="page-item">
		        <a class="page-link" href="<?= $pager->getNext() ?>">Next</a>
		    </li>
        <?php endif ?>
	</ul>
</nav>