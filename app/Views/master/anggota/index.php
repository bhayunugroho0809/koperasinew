<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>
<div class="card">
	<div class="card-header">
		<div class="card-title">Menu</div>
	</div>
	<div class="card-body">
		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
					<?= $this->include('layout/sidebar'); ?>
				</div>
				<div class="col-lg-9 p-2">
					<div class="card">
						<div class="card-header">
							<ul class="list-group list-group-horizontal">
								<li class="list-group-item" data-toggle="tooltip" data-placement="top" title="Buat Data anggota"><a href="#" onClick="return showForm()"><i class="fas fa-pencil-alt text-primary"></i></a></li>
							</ul>
							
						</div>
						<div class="card-body">
							<table class="table table-stripped table-bordered">
								<thead class="bg-primary text-white">
									<tr>
										<th>#</th>
										<th>No</th>
										<th>Nama anggota</th>
										<th>No Anggota</th>
										<th>Tanggal Masuk</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = (!empty($_GET['page_bootstrap']) ? ($_GET['page_bootstrap']-1) * 10 : 0  ); ?>
									<?php foreach($data_list AS $key=>$value): ?>
										<?php $no++ ?>
										<tr>
											<td>
												<div class="form-check">
													<input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="...">
												</div>
											</td>
											<td><?= ($no) ?></td>
											<td><?= $value["nama_anggota"]; ?></td>
											<td><?= $value["no_anggota"]; ?></td>
											<td><?= $value["tanggal_masuk"]; ?></td>
											<td>
												<?php
													$no_anggota = $value["anggota_id"];
												?>
												<a href="#"  onClick="return updateData(event,this)" data-id="<?= $value["anggota_id"] ?>"><i class="fas fa-edit text-success"></i></a> 
												| <a href="#" onClick="return deleteData(event,this)" data-id="<?= $value["anggota_id"] ?>" ><i class="fas fa-trash text-danger"></i></a>
												| <a href="#" onClick="return showPrint('<?= $no_anggota ?>')" ><i class="fas fa-print text-warning"></i></a>
											</td>
										</tr>
										
									<?php endforeach; ?>
								</tbody>
							</table>
							
							<?php echo $pager->links('bootstrap', 'bootstrap_pagination') ?> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade bd-example-modal-lg" id="formanggota" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
  	<form method="post" action="<?= base_url("master_anggota") ?>">	
		<input type="hidden" id="anggota_id" name="anggota_id" />
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah anggota</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
					<?php if (!empty(session()->getFlashdata('error'))) : ?>
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
							<h3><strong>Periksa Entrian Form</strong></h3>
							</hr />
							<?php echo session()->getFlashdata('error'); ?>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					<?php endif; ?>
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<label for="" class="col-sm-3 col-form-label ">NIK anggota</label>
								<div class="col-sm-8">
									<input type="text" class="form-control input-lg" id="nik_anggota" name="nik_anggota" placeholder="NIK anggota">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label required">Nama anggota</label>
								<div class="col-sm-8">
									<input type="text" class="form-control input-lg" required id="nama_anggota" name="nama_anggota" placeholder="Nama anggota">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label required">No Anggota</label>
								<div class="col-sm-8">
								<input type="text" class="form-control" id="no_anggota"  name="no_anggota" placeholder="No Anggota">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label required">Tanggal Masuk</label>
								<div class="col-sm-4">
									<input type="text" class="form-control datepicker" required id="tanggal_masuk" name="tanggal_masuk" placeholder="d/m/Y">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<label for="" class="col-sm-3 col-form-label required">Status</label>
								<div class="col-sm-5 form-inline">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="status" id="statusactive" value="aktif">
										<label class="form-check-label" for="statusactive">Aktif</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="status" id="statusakeluar" value="keluar">
										<label class="form-check-label" for="statusakeluar">Keluar</label>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#datapribadi" role="tab" aria-controls="home" aria-selected="true">Data Pribadi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Data Penjamin</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade p-2 show active" id="datapribadi" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control input-lg" id="alamat" name="alamat" placeholder="Alamat">
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="" class="col-sm-2 col-form-label">RT/RW</label>
                                    <div class="form-inline col-md-3">
                                        <input type="text" class="form-control col-md-5" id="rt" name="rt" placeholder="RT"> / 
                                        <input type="text" class="form-control col-md-5" id="rw" name="rw" placeholder="RW">
                                    </div> 
                                   
                                    <label for="" class="col-auto col-form-label">Kode Pos</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control input-lg" id="kode_pos" name="kode_pos" placeholder="Kode Pos">
                                    </div> 
                                </div>
                                <div class="row ">
                                    <label for="" class="col-sm-2 col-form-label">Kecamatan</label>
                                    <div class="col-sm-4 form-inline">
                                        <select class="form-control" name="kecamatan" style="width:100%">
											<option value="">-Pilih-</option>
											<?php foreach($kecamatan AS $kec): ?>
                                            	<option value="<?php echo $kec['kecamatan_id']; ?>"><?php echo $kec['nama_kecamatan']; ?></option>
											<?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="" class="col-sm-2 col-form-label form-sm require" >Kelompok</label>
                                    <div class="col-sm-4 form-inline">
                                        <select class="form-control" name="kelompok" require id="kelompok" style="width:100%">
											<option value="">-Pilih-</option>
												<?php foreach($kelompok AS $kel): ?>
                                            	<option value="<?php echo $kel['kelompok_id']; ?>"><?php echo $kel['nama_kelompok']; ?></option>
											<?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label form-sm " >Dinas</label>
                                    <div class="col-sm-4 form-inline">
                                        <select class="form-control" name="dinas" style="width:100%">
											<option value="">-Pilih-</option>
											<?php foreach($dinas AS $din): ?>
                                            	<option value="<?php echo $din['dinas_id']; ?>"><?php echo $din['nama_dinas']; ?></option>
											<?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Telepon 1</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control input-lg" id="telepon1" name="telepon1" placeholder="Telepon 1">
                                    </div>
                                </div>
								
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Telepon 2</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control input-lg" id="telepon2" name="telepon2" placeholder="Telepon 2">
                                    </div>
                                </div>
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Tmp/Tanggal Lahir</label>
                                    <div class="col-sm-8 form-inline">
                                        <input type="text" class="form-control input-lg" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat Lahir"> /
										<input type="text" class="form-control col-md-5 datepicker" id="tanggal_lahir" name="tanggal_lahir" placeholder="Tanggal Lahir">
                                    </div>
                                </div>
								<div class="row">
                                    <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                    <div class="col-sm-8 form-inline">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelaminl" value="L">
											<label class="form-check-label" for="inlineRadio1">L</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelaminp" value="P">
											<label class="form-check-label" for="inlineRadio2">P</label>
										</div>
                                    </div>
                                </div>
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Pendidikan</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control input-lg" id="pendidikan" name="pendidikan" placeholder="Pendidikan">
                                    </div>
                                </div>
								
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Pekerjaan</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control input-lg" id="pekerjaan" name="pekerjaan" placeholder="Pendidikan">
                                    </div>
                                </div>
								
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Agama</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control input-lg" id="agama" name="agama" placeholder="Agama">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Status Penjamin</label>
                                    <div class="col-sm-8">
										<select class="form-control" name="data_penjamin" style="width:100%">
                                            <option value="">-Pilih Status Penjamin-</option>
                                            <option value="suami">Suami</option>
                                            <option value="istri">Istri</option>
                                            <option value="anak">Anak</option>
                                            <option value="saudara">Saudara</option>
                                        </select>
                                    </div>
                                </div>
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Nama Penjamin</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control input-lg" id="nama_penjamin" name="nama_penjamin" placeholder="Nama Penjamin">
                                    </div>
                                </div>
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Nik Penjamin</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control input-lg" id="nik_penjamin" name="nik_penjamin" placeholder="Nik Penjamin">
                                    </div>
                                </div>
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Agama Penjamin</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control input-lg" id="agama_penjamin" name="agama_penjamin" placeholder="Agama Penjamin">
                                    </div>
                                </div>
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Telepon Penjamin</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control input-lg" id="telepon_penjamin" name="telepon_penjamin" placeholder="Telepon Penjamin">
                                    </div>
                                </div>
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Alamat Penjamin</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control input-lg" id="alamat_penjamin" name="alamat_penjamin" placeholder="Alamat Penjamin">
                                    </div>
                                </div>
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Alamat Kantor Penjamin</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control input-lg" id="alamat_kantor_penjamin" name="alamat_kantor_penjamin" placeholder="Alamat Kantor Penjamin">
                                    </div>
                                </div>
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Pendidikan Penjamin</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control input-lg" id="pendidikan_penjamin" name="pendidikan_penjamin" placeholder="Pendidikan Penjamin">
                                    </div>
                                </div>
								<div class="row">
                                    <label for="" class="col-sm-2 col-form-label">Pekerjaan Penjamin</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control input-lg" id="pekerjaan_penjamin" name="pekerjaan_penjamin" placeholder="Pekerjaan Penjamin">
                                    </div>
                                </div>
								
							</div>
                        </div>
                    </div>
			</div>
			<div class="modal-footer">
				<button type="submit" name="simpan" id="submit" value="simpan" class="btn btn-primary">Simpan</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</form>
  </div>
</div>

<script>
	function showForm(){
		$("input").val("");
		$("select").val("");
		$(".modal-title").text("Tambah anggota");
		$("#submit").html("Simpan Data");
		$('form').attr('action', '<?= base_url("master_anggota") ?>');
		$("#formanggota").removeData('bs.modal').modal("show");
	}
	
	function updateData(e,identifier){
		e.preventDefault();
		var id = $(identifier).data("id");
		$("input[type='text']").val("");
		$.ajax({
				url: "<?= base_url('master_anggota/detail') ?>", 
				dataType: "json",
				cache: false,
				data:{
					anggota_id : id
				},
				success: function(result){
					if(result.success){
						$("#nik_anggota").val(result.data.nik_anggota);
						$("#nama_anggota").val(result.data.nama_anggota);
						$("#no_anggota").val(result.data.no_anggota);
						$("#anggota_id").val(result.data.anggota_id);
						$("#tanggal_masuk").val(result.data.tanggal_masuk);
						$("#alamat").val(result.data.alamat);
						$("#rt").val(result.data.rt);
						$("#rw").val(result.data.rw);
						$("#telepon1").val(result.data.telepon1);
						$("#telepon2").val(result.data.telepon2);
						$("#agama").val(result.data.agama);
						$("#pendidikan").val(result.data.pendidikan);
						$("#pekerjaan").val(result.data.pekerjaan);
						$("#kode_pos").val(result.data.kode_pos);
						$("#tempat_lahir").val(result.data.tempat_lahir);
						$("input[name='jenis_kelamin'][value='"+ result.data.jenis_kelamin +"']").prop('checked', true);
						$("input[name='status'][value='"+ result.data.status +"']").prop('checked', true);
						$("#tanggal_lahir").val(result.data.tanggal_lahir);
						$("#nama_penjamin").val(result.data.nama_penjamin);
						$("#nik_penjamin").val(result.data.nik_penjamin);
						$("#agama_penjamin").val(result.data.agama_penjamin);
						$("#telepon_penjamin").val(result.data.telepon_penjamin);
						$("#alamat_penjamin").val(result.data.alamat_penjamin);
						$("#alamat_kantor_penjamin").val(result.data.alamat_kantor_penjamin);
						$("#pendidikan_penjamin").val(result.data.pendidikan_penjamin);
						$("#pekerjaan_penjamin").val(result.data.pekerjaan_penjamin);
						$("#kelompok option[value='2']").prop("selected",true);
						$(".modal-title").text("Ubah anggota");
						$("#submit").html("Ubah");
						$('form').attr('action', '<?= base_url("master_anggota/update") ?>');
					}
				}
			});
		$("#formanggota").removeData('bs.modal').modal("show");
	}
	
	function deleteData(e,identifier){
		e.preventDefault();
		var id = $(identifier).data("id");
		Swal.fire({
			title: 'Apakah Yakin?',
			text: "Semua yang terkait dengan data anggota ini akan dihapus",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, Hapus Saja'
		}).then((result) => {
			if (result.isConfirmed) {
				document.location = "<?= base_url("master_anggota/delete/")?>" + "/" + id;
			}
		})
	}

	function showPrint(value) {
		var myWindow = window.open("<?= base_url('master_anggota/print?id='); ?>" + value, "", "width=800,height=700");
       // myWindow.document.write("<p>A new window!</p>");
        myWindow.focus();
		myWindow.onafterprint = function(){
   			alert("bhayu");
			
		}
		//myWindow.close();
       
    }

	$(document).ready(function() {
		<?php if (!empty(session()->getFlashdata('error'))) : ?>
			$("#formanggota").modal("show");
		<?php endif; ?>
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>
<?= $this->endSection() ?>