<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>
<div class="card">
	<div class="card-header">
		<div class="card-title">Menu</div>
	</div>
	<div class="card-body">
		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
					<?= $this->include('layout/sidebar'); ?>
				</div>
				<div class="col-lg-9 p-2">
					<div class="card">
						<div class="card-header">
							<ul class="list-group list-group-horizontal">
								<li class="list-group-item" data-toggle="tooltip" data-placement="top" title="Buat Data Kecamatan"><a href="#" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fas fa-pencil-alt text-primary"></i></a></li>
							</ul>							
						</div>
						<div class="card-body">
							<table class="table table-stripped table-bordered">
								<thead class="bg-primary text-white">
									<tr>
										<th>#</th>
										<th>No</th>
										<th>Nama Kecamatan</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($data_list AS $key=>$value): ?>
										<tr>
											<td>
												<div class="form-check">
													<input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="...">
												</div>
											</td>
											<td><?= ($key+1) ?></td>
											<td><?= $value["nama_kecamatan"]; ?></td>
											<td><a href="#"  onClick="return updateData(event,this)" data-id="<?= $value["kecamatan_id"] ?>"><i class="fas fa-edit text-success"></i></a> | <a href="#" onClick="return deleteData(event,this)" data-id="<?= $value["kecamatan_id"] ?>" ><i class="fas fa-trash text-danger"></i></a></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
							
							<?php echo $pager->links('bootstrap', 'bootstrap_pagination') ?> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade bd-example-modal-lg" id="formkecamatan" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
  	<form method="post" action="<?= base_url("master_kecamatan") ?>">	
		<input type="hidden" id="kecamatan_id" name="kecamatan_id" />
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah Kecamatan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group row">
					<label for="" class="col-sm-3 col-form-label">Nama Kecamatan</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="nama_kecamatan" name="nama_kecamatan" placeholder="Nama Kecamatan">
					</div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</form>
  </div>
</div>
<script>
function updateData(e,identifier){
	e.preventDefault();
	var id = $(identifier).data("id");

	$.ajax({
            url: "<?= base_url('master_kecamatan/detail/') ?>", 
			dataType: "json",
            data:{
                kecamatan_id : id
            },
            success: function(result){
				$("#nama_kecamatan").val(result.nama_kecamatan);
				$("#kecamatan_id").val(result.kecamatan_id);
				$(".modal-title").val("Ubah Kecamatan");
				$('form').attr('action', '<?= base_url("master/kecamatan_update") ?>');
            }
        });
	$("#formkecamatan").modal("show");
}
function deleteData(e,identifier){
	e.preventDefault();
	var id = $(identifier).data("id");
	Swal.fire({
		title: 'Apakah Yakin?',
		text: "Semua yang terkait dengan data kecamatan ini akan dihapus",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ya, Hapus Saja'
	}).then((result) => {
		if (result.isConfirmed) {
			document.location = "<?= base_url("master_kecamatan/delete/")?>" + "/" + id;
		}
	})
}

$(document).ready(function() {
	$('[data-toggle="tooltip"]').tooltip();

	


});
</script>
<?= $this->endSection() ?>