<!DOCTYPE html>
<html>
	<head>
		<!-- CSS only -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
		<link href="<?= base_url('public/css/custom/style.css') ?>" rel="stylesheet" >
		<link href="<?= base_url('public/jquery-ui/jquery-ui.css') ?>" rel="stylesheet" >
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/><!-- JavaScript Bundle with Popper -->
		<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
		<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
		<!------ Include the above in your HEAD tag ---------->
		<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css'>
		<link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Roboto'>
		<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
		<script src="<?= base_url('public/jquery-ui/jquery-ui.js') ?>"></script>
		<script src="<?= base_url('public/js/custom/custom.js') ?>"></script>
		<script src="<?= base_url('public/js/custom/moment.js') ?>"></script>
	</head>
	<body>
		<nav class="navbar p-3 navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="#">
				<img src="<?= base_url('public/img/logo.png') ?>" width="30" height="30" class="d-inline-block align-top" alt="">
				Koperasi
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavDropdown">
				<ul class="navbar-nav">
					<!--<li class="nav-item active">
						<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Features</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Pricing</a>
					</li>-->
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Master Data
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						  <a class="dropdown-item" href="<?= base_url('master_kecamatan') ?>">Daftar Kecamatan</a>
						  <a class="dropdown-item" href="<?= base_url('master_kelompok') ?>">Daftar Kelompok</a>
						  <a class="dropdown-item" href="<?= base_url('master_simpanan') ?>">Jenis Simpanan</a>
						  <a class="dropdown-item" href="<?= base_url('master_pinjaman') ?>">Jenis Pinjaman</a>
						  <a class="dropdown-item" href="<?= base_url('master_anggota') ?>">Anggota</a>
						</div>
					 </li>
					 <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Proses
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						  <a class="dropdown-item" href="<?= base_url('transaksi') ?>">Transaksi Simpan Pinjam</a>
						  <a class="dropdown-item" href="<?= base_url('perhitungan_shu') ?>">Hitung SHU</a>
						</div>
					 </li>
					 <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Laporan
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						  <a class="dropdown-item" href="<?= base_url('laporan_nominatif') ?>">Nominatif</a>
						  <a class="dropdown-item" href="<?= base_url('laporan_harian') ?>">Transaksi Harian</a>
						  <a class="dropdown-item" href="<?= base_url('laporan_bulanan') ?>">Transaksi Bulanan</a>
						  <a class="dropdown-item" href="<?= base_url('laporan_ringkas_bulanan') ?>">Transaksi Ringkas Bulanan</a>
						</div>
					 </li>
					 <li class="nav-item">
						<a class="nav-link" href="#" onClick="return showLogForm()"><i class="fas fa-key"></i> Kunci</a>
					</li>
					 <li class="nav-item">
						<a class="nav-link" href="#"><i class="fas fa-sign-out-alt"></i> Keluar</a>
					</li>
				</ul>
			</div>
		</nav>
		 <?= $this->renderSection('content') ?>
	</body>
		
	<div class="modal fade bd-example-modal-lg" id="formlog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<form method="post" action="<?= base_url("transaksi/kunci_proses") ?>">	
				<input type="hidden" id="dinas_id" name="dinas_id" />
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Kunci Setting</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<label for="" class="col-sm-3 text-right col-form-label">Bulan /Tahun</label>
							<div class="form-inline col-md-9">
								<input type="text" value="" name="bulan" class="form-control col-md-2" placeholder="01-12">&nbsp;/&nbsp; 
								<input type="text" value="" name="tahun" class="form-control-plaintext col-md-3" placeholder="yyyy">
							</div> 
						</div>
						<div class="row">
							<label for="" class="col-sm-3 text-right col-form-label">Keterangan</label>
							<div class="col-md-5">
								<textarea class="form-control" name="keterangan"></textarea>
							</div> 
						</div>
						<div class="row">
							<label for="" class="col-sm-3 text-right col-form-label required">Kunci</label>
							<div class="col-sm-5 form-inline">
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="checkbox" name="previlege_insert" value="Y" checked>
									<label class="form-check-label" for="flexCheckDefault">
										Input
									</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="checkbox" name="previlege_update" value="Y" checked >
									<label class="form-check-label" for="flexCheckChecked">
										Update
									</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="checkbox" name="previlege_delete" value="Y"  checked>
									<label class="form-check-label" for="flexCheckChecked">
										Delete
									</label>
								</div>
							</div>
						</div>
						<center>
							<button type="submit" name="kunci" value="Kunci" class="btn btn-primary">Kunci Perubahan</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						</center>
					</div>
					<div class="modal-footer">
						<table class="table">
							<thead>
								<tr>
									<th>Bulan</th>
									<th>Tahun</th>
									<th>Keterangan</th>
									<th>Kunci Insert</th>
									<th>Kunci Update</th>
									<th>Kunci Delete</th>
									<th>Tgl Dikunci</th>
								</tr>
							</thead>
							<tbody id="body-content-log">
							
							</tbody>
						</table>
						
					</div>
				</div>
			</form>
		</div>
	</div>
</html>
<script>
function showLogForm(){
	list_log();
	$("#formlog").modal("show");
}

function list_log(){        
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '<?= base_url("transaksi/list_log") ?>',
            beforeSend: function(){
            },
            complete: function(){
           
            },
            success: function(data){
                var html = "";
                if(data.success){
                    $.each(data.data,function(index,value){
						html += "<tr>\
							<td>"+ value.bulan +"</td>\
							<td>"+ value.tahun +"</td>\
							<td>"+ value.keterangan +"</td>\
							<td>"+ value.previlege_insert +"</td>\
							<td>"+ value.previlege_update +"</td>\
							<td>"+ value.previlege_delete +"</td>\
							<td>"+ date_format(value.created_at) +"</td>\
						</tr>"
				    });
                    $("#body-content-log").html(html);
                }
            }
         }).fail(function(error) {
             console.log(error);
         });
    }


<?php if(session()->getFlashdata('message_failed')): ?>
	Swal.fire({
		title: 'Error!',
		text: '<?= session()->getFlashdata("message_failed") ?>',
		icon: 'error',
		confirmButtonText: 'OK'
	})
<?php endif; ?>
<?php if(session()->getFlashdata('message_success')): ?>
	Swal.fire({
		title: 'Sukses!',
		text: '<?= session()->getFlashdata("message_success") ?>',
		icon: 'success',
		confirmButtonText: 'OK'
	})
<?php endif; ?>
</script>