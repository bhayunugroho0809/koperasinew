<div class="list-group">
	<?php
		$class_name = get_name_controller();
		$active = "";
		if($class_name == "masterkecamatan"){
			$active = "active ";
		}elseif($class_name == "mastersimpanan"){
			$active = "active ";
		}elseif($class_name == "masterkelompok"){
			$active = "active ";
		}elseif($class_name == "masteranggota"){
			$active = "active ";
		}
	?>
	<a href="<?= base_url('master_kecamatan') ?>" class="<?= ($class_name == "master_kecamatan" ? "active" : "") ?> list-group-item text-center">
		<i class="fas pb-2 fa-building" style="font-size:3em"></i><br/>Daftar Kecamatan
	</a>
	<a href="<?= base_url('master_kelompok') ?>" class="<?= ($class_name == "master_kelompok" ? "active" : "") ?> list-group-item text-center">
		<i class="fa fa-users pb-2" style="font-size:3em"></i><br/>Daftar Kelompok
	</a>
	<a href="<?= base_url('master_dinas') ?>" class="<?= ($class_name == "master_dinas" ? "active" : "") ?> list-group-item text-center">
		<i class="fas fa-id-badge pb-2" style="font-size:3em"></i><br/>Daftar Dinas
	</a>
	<a href="<?= base_url('master_simpanan') ?>" class="<?= ($class_name == "master_simpanan" ? "active" : "") ?> list-group-item text-center">
		<i class="fas fa-money-check pb-2" style="font-size:3em"></i><br/>Jenis Simpanan
	</a>
	<a href="<?= base_url('master_pinjaman') ?>" class="<?= ($class_name == "master_pinjaman" ? "active" : "") ?> list-group-item text-center">
		<i class="fas fa-wallet" style="font-size:3em"></i><br/>Jenis Pinjaman
	</a>
	<a href="<?= base_url('master_anggota') ?>" class="<?=  ($class_name == "master_anggota" ? "active" : "") ?> list-group-item text-center">
		<i class="fa fa-user pb-2" style="font-size:3em"></i><br/>Anggota
	</a>
</div>