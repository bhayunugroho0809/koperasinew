<!DOCTYPE html>
<html>
	<head>
		<!-- CSS only -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
		<link href="<?= base_url('public/css/custom/style.css'); ?>" rel="stylesheet" >
		<!-- JavaScript Bundle with Popper -->
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<!------ Include the above in your HEAD tag ---------->
	</head>
	<body>
		<div class="wrapper fadeInDown">
		  <div id="formContent">
			<!-- Tabs Titles -->

			<!-- Icon -->
			<div class="fadeIn first">
			  <img src="<?= base_url('public/img/logo.png'); ?>" style="width:100px;height:100px"id="icon" alt="User Icon" />
			</div>

			<!-- Login Form -->
			<form method="post">
				<div class="form-group mb-2">
			 	 	<input type="text" id="login" class="fadeIn second form-control p-3" name="username" placeholder="Username">
				</div>
				<div class="form-group mb-4">
					<input type="password" id="password" class="fadeIn third form-control p-3" name="password" placeholder="password">
				</div>
			  	<input type="submit" name="login" class="fadeIn fourth btn btn-primary" value="Log In2">
			</form>

			<!-- Remind Passowrd -->
			<div id="formFooter">
			 Koperasi Dharma Wanita
			</div>

		  </div>
		</div>
	</body>
</html>