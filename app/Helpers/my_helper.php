<?php
 function get_name_controller(){
    $router = service('router'); 
    list($string,$app,$parent_name,$class_name)  = explode('\\',$router->controllerName());   
    $class_name = strtolower($class_name);

    return $class_name;
 }

 
  function format_rupiah($angka){
    $rupiah=number_format($angka,0,',','.');
    return $rupiah;
  }

  function date_indo($date = null){
    if(!$date){
      return null; 
    }
    list($thn,$bln,$tgl,) = explode("-",$date);
    $tanggal_indo = $tgl."/".$bln."/".$thn;

    return $tanggal_indo;
  }

  function date_database($date = null){
    if(!$date){
      return null; 
    }
    list($tgl,$bln,$thn) = explode("/",$date);
    $tanggal_database = $thn."-".$bln."-".$tgl;

    return $tanggal_database;
  }

  function list_bulan(){
    $bulan = ["01"=>"Januari","02"=>"Februari","03"=>"Maret","04"=>"April","05"=>"Mei","06"=>"Juni","07"=>"Juli","08"=>"Agustus","09"=>"September","10"=>"Oktober","11"=>"November","12"=>"Desember"];

    return $bulan;
  }
?>