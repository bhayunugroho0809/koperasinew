
$(document).ready(function() {
    $('.select2').select2({
		width: 'resolve' 
	});

    $( ".datepicker" ).datepicker({
		dateFormat:"dd/mm/yy",
		changeMonth: true,
		changeYear: true
	});
});

function selectopt_basic(id_or_class){
		
	var dropdown = $(id_or_class).select2();

	return dropdown;
}

function selectopt(id_or_class, txtplaceholder, url_ajax, multiple_select=false){
		
	var dropdown = $(id_or_class).select2({
		placeholder: txtplaceholder,
		tags: false,
		multiple: multiple_select,
		allowClear: true,
		createTag: function (tag) {
			return {
					id: tag.term,
					text: tag.term,
					isNew : false
				};
			},
			ajax: {
				url: url_ajax,
				dataType: 'json',
				delay: 200,
				type: "GET",
				quietMillis: 50,
				data: function (term,page) {
					
					return {
						p: term,
						page: page
					};
				},
				processResults: function (data, page) {
					return {
                        results: $.map(data, function(country) {
                            return { id: country.id, code: country.code, text: country.name };
                        })
                    };
				},
				error: function(jqxhr,textStatus,errorThrown)
                {
					console.log(textStatus);
				},
				cache: false,
			}			
		});
	
	return dropdown;
}

function date_format(dateString){
	if(!dateString){
		return '';
	}
	var d = new Date(dateString);
	var strDate = moment(dateString).format('DD/MM/YYYY')

	return strDate;
}

function number_format(value) {
	if(!value){
		return 0;
	}
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}